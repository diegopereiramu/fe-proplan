// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const HOST_SSBIOBIO = 'https://saludbiobio.devesteam.com/api/'
export const environment = {
  production: false,
  microservices: {
    login : {
      auth: 'http://localhost:8004'+'/',
      validateSession: 'http://localhost:8004'+'/session/validate'
    },
    professional: {
      getProfessional: 'http://localhost:8001'+'/',
      getAllProfesional: 'http://localhost:8001'+'/listado'
    },
    contract: {
      getContractByIdProfessional: 'http://localhost:8002/'+'profesional/',
      creacionExtension: 'http://localhost:8002/'+'nueva-extension'
    },
    programming: {
      getProgrammingByContract: 'http://localhost:8003'+'/contrato/',
      addActivityToContract: 'http://localhost:8003'+'/',
      changeStatus: 'http://localhost:8003'+'/',
      porcentajeProgramacion:'http://localhost:8003'+'/obtener-programaciones/',
      obtenerResumenLey: 'http://localhost:8003'+'/obtener-resumen/'
    },
    catalogue: {
      getAllSpecialty: 'http://localhost:8006/'+'especialidades',
      getSpecialtyActive: 'http://localhost:8006/'+'especialidadesActive',
      deleteSpeciality: 'http://localhost:8006/'+'change-status-especialidad/',
      getActivityBySpecialty: 'http://localhost:8006/'+'actividades/',
      getAllActivities: 'http://localhost:8006/'+'actividades/',
      getAllCostCenters: 'http://localhost:8006/'+'centro-costos/',
      addActivity: 'http://localhost:8006/'+'actividades',
      addCostCenter: 'http://localhost:8006/'+'centro-costos',
      getAllActivityTypes: 'http://localhost:8006/'+'actividades/tipos',
      getPeriods: 'http://localhost:8006/'+'periodos',
      getCostCenter: 'http://localhost:8006/'+'centro-costos',
      getCostCenterbyUser: 'http://localhost:8006/'+'centro-costo',
      getLeyes: 'http://localhost:8006/'+'leyes',
      addLaw: 'http://localhost:8006/'+'leyes',
      getTitulos: 'http://localhost:8006/'+'titulos',
      getUsuarios: 'http://localhost:8006/'+'usuariosFull',
      postUsuario: 'http://localhost:8006/'+'usuariosFull',
      putUsuario: 'http://localhost:8006/'+'usuarioFull',
      putUsuarios : 'http://localhost:8006/'+'usuariosFull',
      getRoles: 'http://localhost:8006/'+'roles',
    },
    synchronizer: 'http://localhost:8000/'+'rrhh',
    synchronizerOne: 'http://localhost:8000/'+'rrhh-save',
    diasFeriados:' http://apis.digital.gob.cl/fl/feriados/',
    exportExcel: 'http://localhost:8000/excel',
  }
};