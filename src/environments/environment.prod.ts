const HOST_SSBIOBIO = 'http://51.75.169.85/api/'
export const environment = {
  production: true,
  microservices: {
    login : {
      auth: HOST_SSBIOBIO+'auth/',
      validateSession: HOST_SSBIOBIO+'auth/session/validate'
    },
    professional: {
      getProfessional: HOST_SSBIOBIO+'profesionales/',
      getAllProfesional: HOST_SSBIOBIO+'profesionales/listado'
    },
    contract: {
      getContractByIdProfessional: HOST_SSBIOBIO+'contratos/profesional/',
      creacionExtension: HOST_SSBIOBIO+'contratos/nueva-extension'
    },
    programming: {
      getProgrammingByContract: HOST_SSBIOBIO+'programaciones/contrato/',
      addActivityToContract: HOST_SSBIOBIO+'programaciones/',
      changeStatus: HOST_SSBIOBIO+'programaciones/',
      porcentajeProgramacion:HOST_SSBIOBIO+'programaciones/obtener-programaciones/',
      obtenerResumenLey: HOST_SSBIOBIO+'programaciones/obtener-resumen/'
    },
    catalogue: {
      getAllSpecialty: HOST_SSBIOBIO+'catalogos/especialidades',
      deleteSpeciality: HOST_SSBIOBIO+'catalogos/change-status-especialidad/',
      getActivityBySpecialty: HOST_SSBIOBIO+'catalogos/actividades/',
      getAllActivities: HOST_SSBIOBIO+'catalogos/actividades/',
      getAllCostCenters: HOST_SSBIOBIO+'catalogos/centro-costos/',
      addActivity: HOST_SSBIOBIO+'catalogos/actividades/',
      addCostCenter: HOST_SSBIOBIO+'catalogos/centro-costos/',
      getAllActivityTypes: HOST_SSBIOBIO+'catalogos/actividades/tipos',
      getPeriods: HOST_SSBIOBIO+'catalogos/periodos',
      getCostCenter: HOST_SSBIOBIO+'catalogos/centro-costos',
      getLeyes: HOST_SSBIOBIO+'catalogos/leyes',
      addLaw: HOST_SSBIOBIO+'catalogos/leyes',
      getTitulos: HOST_SSBIOBIO+'catalogos/titulos',
    },
    synchronizer: HOST_SSBIOBIO+'sincronizador/rrhh',
    synchronizerOne: HOST_SSBIOBIO+'sincronizador/rrhh-save',
    exportExcel: HOST_SSBIOBIO+'sincronizador/excel',
    diasFeriados:' https://apis.digital.gob.cl/fl/feriados/'
  }
};
