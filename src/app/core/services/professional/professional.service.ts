import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { Professional } from './../../models/professional/professional';
import { DecimalPipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { SortColumn, SortDirection } from './../../directives/sortable.directive';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface SearchResult {
  professional: Professional[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(professional: Professional[], column: SortColumn, direction: string): Professional[] {
  if (direction === '' || column === '') {
    return professional;
  } else {
    return [...professional].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(professional: Professional, term: string) {
  return professional.nombre.toLowerCase().includes(term.toLowerCase())
}

@Injectable({ providedIn: 'root' })
export class ProfessionalService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _professional$ = new BehaviorSubject<Professional[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private PROFESSIONAL: Professional[] = [];
  private _state: State = {
    page: 1,
    pageSize: 6,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private http: HttpClient) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._professional$.next(result.professional);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get professional$() { return this._professional$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({ page }); }
  set pageSize(pageSize: number) { this._set({ pageSize }); }
  set searchTerm(searchTerm: string) { this._set({ searchTerm }); }
  set sortColumn(sortColumn: SortColumn) { this._set({ sortColumn }); }
  set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

    // 1. sort
    let professional = sort(this.PROFESSIONAL, sortColumn, sortDirection);

    // 2. filter
    professional = professional.filter(professionals => matches(professionals, searchTerm));
    const total = professional.length;

    // 3. paginate
    professional = professional.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({ professional, total });
  }

  public getProfessional(idPeriod, cc): Observable<any> {
    let params = new HttpParams();
    params = params.append('idPeriodo', idPeriod);
    if (cc != null) {
      params = params.append('centrosCostos', cc);
    }
    return this.http.get(environment.microservices.professional.getProfessional, { params: params }).pipe(map((response: any) => {
      if (response.OK) {
        this.PROFESSIONAL = response.data;
        this.PROFESSIONAL.forEach(profesional => {
          profesional.porcentaje = 0;
        })
        this._search$.next();
        return this.PROFESSIONAL;
      } else {
      }
    }))
  }
  getProgrammingPorcentage(idPeriodo) {
    return this.http.get(environment.microservices.programming.porcentajeProgramacion + idPeriodo).pipe(map((response: any) => {
      const result = response.programacion.reduce((r, { id, horas_programadas, horas_semanales }) => {
        r[id] = r[id] || { id: id, horas_programadas: 0, horas_semanales: 0 }
        r[id].horas_programadas += horas_programadas
        r[id].horas_semanales += horas_semanales
        return r
      }, {})
      let array: any = Object.values(result)
      this.PROFESSIONAL.forEach(profesional => {
        let porcentaje = array.filter(porcentaje => porcentaje.id == profesional.id);
        let division = ((porcentaje[0].horas_programadas * 100) / porcentaje[0].horas_semanales).toFixed(2);
        profesional.porcentaje = division;
      })
      return this.PROFESSIONAL;
    }))
  }
  getProfesional() {
    return this.http.get(environment.microservices.professional.getAllProfesional).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }
  creacionProfesional(data) {
    return this.http.post(environment.microservices.synchronizerOne, data).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }
}
