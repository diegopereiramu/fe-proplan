import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Activity } from './../../models/activity/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  private _search$ = new Subject<void>();
  constructor(private http:HttpClient) { }

  public getActivityBySpecialty(idSpecialty, typeActivity): Observable<any>  {
    let params = new HttpParams();
    if(idSpecialty != null) {
      params = params.append('especialidad', idSpecialty);
    }
    params = params.append('tipoActividad', typeActivity);
    return this.http.get(environment.microservices.catalogue.getActivityBySpecialty, { params: params }).pipe(map((response: any) => {
        if(response.OK) {
          return response
        }
    }))
  }

  public getActivitiesCatalogue(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getAllActivities).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }

  public getActivityTypesCatalogue(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getAllActivityTypes).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }

  public addActivity(data): Observable<any> {
    return this.http.post(environment.microservices.catalogue.addActivity, data).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }

  public updateActivity(data, id): Observable<any> {
    return this.http.put(environment.microservices.catalogue.addActivity+'/'+id, data).pipe(map((response: any) => {
        return response
    }))
  }

  public deleteActivity(id) {
    return this.http.delete(environment.microservices.catalogue.addActivity+'/'+id).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }

  public activeActivity(id) {
    return this.http.put(environment.microservices.catalogue.addActivity+'/'+id+'/activar', null).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
}


 
