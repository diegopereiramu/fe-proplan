import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { user } from './../../models/user/user';
@Injectable({
  providedIn: 'root'
})

export class AuthService {


  constructor(private http: HttpClient) {
  }
  auth(email, password): Observable<user> {
    const request = {
      user: email,
      pass: password
    }
    return this.http.post(environment.microservices.login.auth, request).pipe(map((response: any) => {
      console.log(response)
      if (!response.OK) {
        return response;
      } else {
        let objectLocalStorage = {
          name: response.data.user.nombre,
          email: response.data.user.usuario,
          idUser: response.data.user.id,
          activo: response.data.user.activo,
          rols: response.data.roles,
          session: response.data.session
        };
        localStorage.setItem('Personal-data', JSON.stringify(objectLocalStorage));
        return response;
      }
    }))
  }
}
