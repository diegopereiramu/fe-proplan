import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComunicationService {
  private changePeriods = new Subject<any>();
  private openAndClose = new Subject<any>();
  constructor() { }

  listenChangePeriods(idVersion:any) {
    this.changePeriods.next(idVersion);
  }
  listenChangePeriod(): Observable<any> {
    return this.changePeriods.asObservable();
  }
  closeAndOpen(idVersion:string = 'hola') {
    this.openAndClose.next(idVersion);
  }
  listencloseAndOpen(): Observable<any> {
    return this.openAndClose.asObservable();
  }
}
