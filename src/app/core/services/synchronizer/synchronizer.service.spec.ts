import { TestBed } from '@angular/core/testing';

import { SynchronizerService } from './synchronizer.service';

describe('SynchronizerService', () => {
  let service: SynchronizerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SynchronizerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
