import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SynchronizerService {

  constructor(private http:HttpClient) { }

  public synchronize(file): Observable<any>  {
    const formData: FormData = new FormData();
    formData.append('excel', file, file.name);
    return this.http.post(environment.microservices.synchronizer, formData).pipe(map((response: any) => {
        console.log(response)
        if(response.OK) {
          return response
        }
    }))
  }
}


 
