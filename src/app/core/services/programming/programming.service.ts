import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { Programming } from './../../models/programming/programming';
import { DecimalPipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { SortColumn, SortDirection } from './../../directives/sortable.directive';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface SearchResult {
  programming: Programming[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(programming: Programming[], column: SortColumn, direction: string): Programming[] {
  if (direction === '' || column === '') {
    return programming;
  } else {
    return [...programming].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(programming: Programming, term: string) {
  return programming.actividad.descripcion.toLowerCase().includes(term.toLowerCase())
}
@Injectable({
  providedIn: 'root'
})
export class ProgrammingService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _programming$ = new BehaviorSubject<Programming[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private PROGRAMMING: Programming[] = [];
  private _state: State = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private http: HttpClient) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._programming$.next(result.programming);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get programming$() { return this._programming$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({ page }); }
  set pageSize(pageSize: number) { this._set({ pageSize }); }
  set searchTerm(searchTerm: string) { this._set({ searchTerm }); }
  set sortColumn(sortColumn: SortColumn) { this._set({ sortColumn }); }
  set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

    // 1. sort
    let programming = sort(this.PROGRAMMING, sortColumn, sortDirection);

    // 2. filter
    programming = programming.filter(programmings => matches(programmings, searchTerm));
    const total = programming.length;

    // 3. paginate
    programming = programming.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({ programming, total });
  }

  public getProgrammingByContractExtension(idContract): Observable<any> {
    return this.http.get(environment.microservices.programming.getProgrammingByContract + idContract).pipe(map((response: any) => {
      if (response.OK) {
        this.PROGRAMMING = response.programaciones;
        this._search$.next();
        return this.PROGRAMMING;
      }
    }))
  }
  public addActivityToContract(request): Observable<any> {
    return this.http.post(environment.microservices.programming.addActivityToContract, request).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }

  updateActivityInContract(request): Observable<any> {
    return this.http.put(environment.microservices.programming.addActivityToContract + request.id, request).pipe(map((response: any) => {
      console.log(response);
      if (response.OK) {
        return response;
      }
    }))
  }

  changeStatus(idProgramming, status) {
    let req = { "estado": status }
    return this.http.put(environment.microservices.programming.changeStatus + idProgramming + '/cambiar-estado-aprobacion', req).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
  deleteProgramming(idProgramming) {
    return this.http.delete(environment.microservices.programming.changeStatus + idProgramming).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
  getDiasFeriado(anno) {
    return this.http.get(environment.microservices.diasFeriados + anno).pipe(map((response: any) => {
      return response;
    }))
  }

  getResumenLey(request): Observable<any>{
    return this.http.post(environment.microservices.programming.obtenerResumenLey,request).pipe(map((response: any) => {
      console.log(response);
      if(response.OK){
        return response.data;
      }
    }))
  }
}
