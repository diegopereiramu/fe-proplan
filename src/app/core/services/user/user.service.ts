import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { user2 } from '../../models/user/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) {}
    
  public getUsers(): Observable<any> {
      return this.http.get(environment.microservices.catalogue.getUsuarios).pipe(map((response: any) => {
        if (response.OK) {
          return response.data  
        }
      }))
  }
  public postUser(nuevo): Observable<any> {
    return this.http.post(environment.microservices.catalogue.postUsuario,nuevo).pipe(map((response: any) => {
      if (response.OK) {
        return response.data;
      }
    }))
  }
  public putUser(nuevo): Observable<any> {
    return this.http.put(environment.microservices.catalogue.putUsuario,nuevo).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }
  public deleteUser(nuevo): Observable<any> {
    return this.http.put(environment.microservices.catalogue.putUsuarios,nuevo).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }
}
