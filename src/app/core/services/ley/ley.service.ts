import { Injectable} from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LeyService {

  constructor(private http:HttpClient) { }

  public getLey(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getLeyes).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }

  public getLawsCatalogue(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.addLaw).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }

  public addLaw(data): Observable<any> {
    return this.http.post(environment.microservices.catalogue.addLaw, data).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }

  public updateLaw(data, id): Observable<any> {
    return this.http.put(environment.microservices.catalogue.addLaw+'/'+id, data).pipe(map((response: any) => {
        return response
    }))
  }

  public deleteLaw(id) {
    return this.http.delete(environment.microservices.catalogue.addLaw+'/'+id).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }

  public activeLaw(id) {
    return this.http.put(environment.microservices.catalogue.addLaw+'/'+id+'/activar', null).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
}
