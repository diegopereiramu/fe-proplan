import { Injectable, PipeTransform } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { Specialty } from './../../models/specialty/specialty';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { SortColumn, SortDirection } from './../../directives/sortable.directive';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface SearchResult {
  specialty: Specialty[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(specialty: Specialty[], column: SortColumn, direction: string): Specialty[] {
  if (direction === '' || column === '') {
    return specialty;
  } else {
    return [...specialty].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(specialty: Specialty, term: string) {
  return specialty.descripcion.toLowerCase().includes(term.toLowerCase())
}

@Injectable({
  providedIn: 'root'
})
export class SpecialtyService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _specialty$ = new BehaviorSubject<Specialty[]>([]);
  private _total$ = new BehaviorSubject<number>(0);
  private SPECIALTY: Specialty[] = [];
  private _state: State = {
    page: 1,
    pageSize: 6,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private http: HttpClient) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(0),
      switchMap(() => this._search()),
      delay(0),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._specialty$.next(result.specialty);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get specialty$() { return this._specialty$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({ page }); }
  set pageSize(pageSize: number) { this._set({ pageSize }); }
  set searchTerm(searchTerm: string) { this._set({ searchTerm }); }
  set sortColumn(sortColumn: SortColumn) { this._set({ sortColumn }); }
  set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const { sortColumn, sortDirection, pageSize, page, searchTerm } = this._state;

    // 1. sort
    let specialty = sort(this.SPECIALTY, sortColumn, sortDirection);

    // 2. filter
    specialty = specialty.filter(specialties => matches(specialties, searchTerm));
    const total = specialty.length;

    // 3. paginate
    specialty = specialty.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({ specialty, total });
  }

  public getSpecialtyCatalogue(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getAllSpecialty).pipe(map((response: any) => {
      if (response.OK) {
        this.SPECIALTY = response.especialidades;
        this._search$.next();
        return this.SPECIALTY;
      }
    }))
  }

  public getSpecialty(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getAllSpecialty).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }

  public getSpecialtyActive(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getSpecialtyActive).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }
  
  public addSpecialty(data): Observable<any> {
    return this.http.post(environment.microservices.catalogue.getAllSpecialty, data).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }
  public updateSpecialty(data, id): Observable<any> {
    let params = new HttpParams();
    params = params.append('id', id);
    return this.http.put(environment.microservices.catalogue.getAllSpecialty+'/'+id, data).pipe(map((response: any) => {
        return response
    }))
  }

  deleteSpeciality(data, id) {
    return this.http.put(environment.microservices.catalogue.deleteSpeciality+id, data).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
}

