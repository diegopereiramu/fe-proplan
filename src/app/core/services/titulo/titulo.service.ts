import { Injectable} from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TituloService {

  constructor(private http:HttpClient) { }

  public getTitulos(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getTitulos).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }
}
