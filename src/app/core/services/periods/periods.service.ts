import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Periods } from './../../models/periods/periods';

@Injectable({
  providedIn: 'root'
})
export class PeriodsService {

  constructor(private http:HttpClient) { }

  public getPeriods(): Observable<any>  {
    return this.http.get(environment.microservices.catalogue.getPeriods).pipe(map((response: any) => {
        if(response.OK) {
          return response
        }
    }))
  }
}
 
