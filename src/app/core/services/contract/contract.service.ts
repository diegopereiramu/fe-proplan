import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Contract } from './../../models/contract/contract';
@Injectable({
  providedIn: 'root'
})
export class ContractService {
  
  constructor(private http:HttpClient) { }
  
  public getContractByIdProfessional(idProfesional, idPeriod): Observable<any>  {
    let params = new HttpParams();
    params = params.append('idPeriodo', idPeriod);
    return this.http.get(environment.microservices.contract.getContractByIdProfessional+idProfesional, {params:params}).pipe(map((response: any) => {
        if(response.OK) {
          return response
        }
    }))
  }

  public creacionExtension(data): Observable<any>  {
    return this.http.post(environment.microservices.contract.creacionExtension, data).pipe(map((response: any) => {
        if(response.OK) {
          return response
        }
    }))
  }
}
