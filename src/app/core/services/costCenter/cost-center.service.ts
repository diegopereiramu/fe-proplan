import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Contract } from './../../models/contract/contract';

@Injectable({
  providedIn: 'root'
})
export class CostCenterService {

  constructor(private http:HttpClient) { }

  public getCostCenterByUser(isAdmin,idUser): Observable<any>  {
    let params = new HttpParams();
    if(isAdmin != true) {
        params = params.append('id', idUser);
        return this.http.get(environment.microservices.catalogue.getCostCenterbyUser, {params:params}).pipe(map((response: any) => {
          if(response.OK) {
            return response
          }
      }))
    }
    else{
        return this.http.get(environment.microservices.catalogue.getAllCostCenters).pipe(map((response: any) => {
          if (response.OK) {
            return response;
          }
        }))
    }
  } 

  public getCc(): Observable<any>  {
    return this.http.get(environment.microservices.catalogue.getCostCenter).pipe(map((response: any) => {
        if(response.OK) {
          return response
        }
    }))
  }

  public getCostCenterCatalogue(): Observable<any> {
    return this.http.get(environment.microservices.catalogue.getAllCostCenters).pipe(map((response: any) => {
      if (response.OK) {
        return response;
      }
    }))
  }

  public addCostCenter(data): Observable<any> {
    return this.http.post(environment.microservices.catalogue.addCostCenter, data).pipe(map((response: any) => {
      if (response.OK) {
        return response
      }
    }))
  }

  public updateCostCenter(data, id): Observable<any> {
    return this.http.put(environment.microservices.catalogue.addCostCenter+'/'+id, data).pipe(map((response: any) => {
        return response
    }))
  }

  public deleteCostCenter(id) {
    return this.http.delete(environment.microservices.catalogue.addCostCenter+'/'+id).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }

  public activeCostCenter(id) {
    return this.http.put(environment.microservices.catalogue.addCostCenter+'/'+id+'/activar', null).pipe(map((response: any) => {
      console.log(response)
      if (response.OK) {
        return response;
      }
    }))
  }
  
}
 