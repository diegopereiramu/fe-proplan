import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor() { }

  getDays(inicio, fin, feriados) {
    // let inicio = moment(this.fechaInicio, "DD-MM-YYYY");
    // let fin = moment(this.fechaFin, "DD-MM-YYYY");
    let diasTotalesAnual = fin.diff(inicio, 'days') + 1;
    //let diasFinDeSemana = this.getWeekDays(inicio, fin);
    let diasFeriados = this.feriados(feriados, inicio, fin);
    //let diasTotalesConDescuentos = diasTotalesAnual - diasFinDeSemana - diasFeriados
    let diasTotalesConDescuentos = diasTotalesAnual - diasFeriados;
    return diasTotalesConDescuentos;
  }
  //DIAS
  getWeekDays(inicio, fin) {
    const start = inicio
    const end = fin //today
    //calculate only Tuesday
    const dailyInfo = [true, false, false, false, false, false, true]
    let totalDays = 0;
    dailyInfo.forEach((info, index) => {
      if (info === true) {
        let current = start.clone();
        if (current.isoWeekday() <= index) {
          current = current.isoWeekday(index);
        } else {
          current.add(1, 'weeks').isoWeekday(index);
        }
        while (current.isSameOrBefore(end)) {
          current.day(7 + index);
          totalDays += 1;
        }
      }
    });
    return totalDays;
  }
  //DIAS LIBRES 
  feriados(feriados, inicio, fin) {
    //ELiminar fechas que esten fuera del rango
    let feriadosNoFinSemana = 0;
    feriados.forEach(feriados => {
      if (moment(feriados.fecha) >= inicio && moment(feriados.fecha) <= fin) {
        if (feriados.nombre != "Todos los Días Domingos") {
          let day: any = moment(feriados.fecha).format('dddd');
          if (day == "Monday" || day == "Tuesday" || day == "Wednesday" || day == "Thursday" || day == "Friday") {
            feriadosNoFinSemana += 1;
          }
        }
      }
    });
    return feriadosNoFinSemana;
  }

  daysOfYear(year) {
    return this.isLeapYear(year) ? 366 : 365;
  }
  
  isLeapYear(year) {
    return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);
  }
}
