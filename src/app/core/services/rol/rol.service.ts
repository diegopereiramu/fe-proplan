import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  constructor(private http:HttpClient) { }

  public getRoles(): Observable<any>  {
    return this.http.get(environment.microservices.catalogue.getRoles).pipe(map((response: any) => {
        if(response.OK) {
          return response.data
        }
    }))
  }
}
