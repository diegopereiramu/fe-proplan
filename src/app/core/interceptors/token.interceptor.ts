import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpClient
} from '@angular/common/http';
import { AuthService } from './../services/auth/auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    public authService: AuthService, private route: Router, private http: HttpClient, public spinner:NgxSpinnerService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let dataUserLocalStorage = localStorage.getItem('Personal-data');
    let token = localStorage.getItem('api-token');
    if (token) {
      request = request.clone({
        setHeaders: {
          'Authorization': 'Bearer ' + token
        }
      });
    }
    let newReq = request.clone();
    return next.handle(request).pipe(catchError((error: HttpErrorResponse) => {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
      } else if (error.status == 403) {
        if (dataUserLocalStorage) {
          let data = { "session": JSON.parse(dataUserLocalStorage)['session'] }
          return this.http.post(environment.microservices.login.validateSession, data).pipe(mergeMap((response: any) => {
            if (response.OK) {
              localStorage.setItem('api-token', response.data.token);
              newReq = request.clone({
                setHeaders: {
                  'Authorization': 'Bearer ' + response.data.token
                }
              });
              return next.handle(newReq)
            } else if (!response.OK) {
              this.spinner.hide();
              Swal.fire({
                title: 'Sesión expirada',
                text: 'Por favor presione "Ir a inicio de sesión", para volver a ingresar al sistema ',
                icon: 'error',
                confirmButtonText: 'Ir a inicio de sesion',
                allowOutsideClick: false,
                allowEscapeKey: false
              }).then((result) => {
                if (result.value) {
                  this.route.navigateByUrl('/');
                }
              })
            }
          }))
        } else {
          this.spinner.hide();
          Swal.fire({
            title: 'Sesión expirada',
            text: 'Por favor presione "Ir a inicio de sesión", para volver a ingresar al sistema ',
            icon: 'error',
            confirmButtonText: 'Ir a inicio de sesion',
            allowOutsideClick: false,
            allowEscapeKey: false
          }).then((result) => {
            if (result.value) {
              this.route.navigateByUrl('/');
            }
          })
        }

      } else {
      }
      return throwError(errorMessage);
    })
    );
  }
}
