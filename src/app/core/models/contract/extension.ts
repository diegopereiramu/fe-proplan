export class Extension {
    activo: boolean;
    dias: dias;
    fecha_creacion_Registro: string;
    fecha_inicio: string;
    fecha_termino: string;
    horas_semanales:number;
    id: number;
    sistema_turno: boolean;
    tiempo: tiempo;
}
class dias {
    congreso_capacitacion: number;
    descanco_compensatorio: number;
    feriados_legales: number;
    permiso_administrativo: number;
}
class tiempo {
    colacion_semanal: number;
    lactancia_semanal: number;
}