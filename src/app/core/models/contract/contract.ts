import { ley } from './../law/law';
import { CostCenter } from './../costCenter/costCenter';
import { Extension } from './../contract/extension';
export class Contract {
    activo: boolean;
    centro_costos:CostCenter;
    codigo: number;
    especialidad_sis: string;
    extensiones: Extension;
    id: number;
    inicio:string;
    ley:ley;
    profesional_id: number;
    termino:string;
}