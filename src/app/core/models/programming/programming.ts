import { Specialty } from './../specialty/specialty';
export class Programming {
    id: number;
    actividad: actividad;
    especialidad: Specialty;
    centro_costo: centro_costo;
    horas: horas;
    fecha_registro:string;
}
class actividad {
    id: number;
    codigo: string;
    descripcion: string;
}
class centro_costo {
    id: number;
    codigo: string;
    descripcion: string;
}
class horas {
    asignadas: number;
    rendimiento: number;
    legales_descuento: number;
}
