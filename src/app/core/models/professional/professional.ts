export class Professional {
    id: number;
    documento: document;
    nombre: string;
    titulos: title;
    activo: boolean;
    tipo_actividad:string;
    porcentaje:any;
    ley: string;
}
class document {
    numero: string;
    tipo: string;
}
class title {
    id: number;
    descripcion: string;
}
class contract{
    codigo:number;
    especialidad_sis:string;
    fecha_inicio:string;
    fecha_termino:string;
}
class contract_extension{

}

/*let obj = {
    nombre: this.inputNombre,
    documento: {
      numero: rutNombnres,
      tipo: 'RUT'
    },
    titulos: this.selectTituloProfesional,
    contratos: {
      ley: this.selectLey,
      codigo: this.inputCodigoCorrelativo,
      especialidad_sis: this.inputEspecialidad === '' || this.inputEspecialidad == '0' || this.inputEspecialidad == null || this.inputEspecialidad == undefined ? null : this.inputEspecialidad,
      horas_semanales: this.inputHoras,
      sistema_turno: this.selectTurnos == 'S' ? true : false,
      dias: {
        feriados_legales: this.inputFeriados === undefined ? 0 : this.inputFeriados,
        descanso_compensatorio: this.inputDescanso === undefined ? 0 : this.inputDescanso,
        permiso_administrativo: this.inputPermisos === undefined ? 0 : this.inputPermisos,
        congreso_capacitacion: this.inputCongreso === undefined ? 0 : this.inputCongreso,
      },
      tiempo: {
        lactancia_semanal: this.inputLactancia === undefined ? 0 : this.inputLactancia,
        colacion_semanal: this.inputColacion === undefined ? 0 : this.inputColacion
      },
      inicio: this.txtDesde.year + '/' + this.txtDesde.month + '/' + this.txtDesde.day,
      termino: this.txtHasta.year + '/' + this.txtHasta.month + '/' + this.txtHasta.day,
      centro_costos: cc
    },
    fechaInicioProgramacion: this.txtDesdeProgramacion.year + '/' + this.txtDesdeProgramacion.month + '/' + this.txtDesdeProgramacion.day,
    fechaFinProgramacion: this.txtHastaProgramacion.year + '/' + this.txtHastaProgramacion.month + '/' + this.txtHastaProgramacion.day
  }*/