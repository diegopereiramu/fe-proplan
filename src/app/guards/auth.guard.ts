import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let hasAccess = false;
      if (localStorage.getItem('Personal-data')){
        const personalData = JSON.parse(localStorage.getItem('Personal-data'));
        personalData.rols.forEach(r => {
          if(r.codigo === "ADMINISTRADOR" && personalData.activo){
            hasAccess =  true;
          }
        });
      }
    return hasAccess;
  }
  
}
