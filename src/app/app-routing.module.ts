import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './features/login/login.component';
import { HomeComponent } from './features/home/home.component';
import { ProgrammingComponent } from './features/programming/programming.component';
import { SynchronizerComponent } from './features/synchronizer/synchronizer.component';
import { SpecialtyComponent } from './features/specialty/specialty.component';
import { ProfessionalComponent } from './features/professional/professional.component';
import { ActivityComponent } from './features/activity/activity.component';
import { CostCenterComponent } from './features/cost-center/cost-center.component';
import { LawComponent } from './features/law/law.component';
import { ContractComponent } from './features/contract/contract.component';
import { UserComponent } from './features/user/user.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent,  },
  { path: 'programacion', component: ProgrammingComponent },
  { path: 'sincronizador', component: SynchronizerComponent, canActivate: [AuthGuard] },
  { path: 'especialidad', component: SpecialtyComponent, canActivate: [AuthGuard] },
  { path: 'profesional', component: ProfessionalComponent, canActivate: [AuthGuard] },
  { path: 'actividad', component: ActivityComponent, canActivate: [AuthGuard] },
  { path: 'centro-costo', component: CostCenterComponent, canActivate: [AuthGuard] },
  { path: 'leyes', component: LawComponent, canActivate: [AuthGuard] },
  { path: 'contrato', component: ContractComponent, canActivate: [AuthGuard] },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }