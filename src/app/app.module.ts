import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../app/core/interceptors/token.interceptor';
import { AppComponent } from './app.component';
import { LoginComponent } from './features/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from 'ngx-pagination'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarComponent } from './features/sidebar/sidebar.component';
import { HomeComponent } from './features/home/home.component';
import { NavbarComponent } from './features/navbar/navbar.component';
import { ProgrammingComponent } from './features/programming/programming.component';
import { SynchronizerComponent } from './features/synchronizer/synchronizer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng9RutModule } from 'ng9-rut';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { ModalProgrammingComponent } from './features/programming/components/modal-programming/modal-programming.component';
import { FormActivityComponent } from './features/programming/components/form-activity/form-activity.component';
import { SidebarModule } from 'ng-sidebar';
import { SpecialtyComponent } from './features/specialty/specialty.component';
import { ModalActivityComponent } from './features/activity/components/modal-activity/modal-activity.component';
import { ModalCostCenterComponent } from './features/cost-center/components/modal-cost-center/modal-cost-center.component';
import { ModalLawComponent } from './features/law/components/modal-law/modal-law.component';
import { ProfessionalComponent } from './features/professional/professional.component';
import { ActivityComponent } from './features/activity/activity.component';
import { CostCenterComponent } from './features/cost-center/cost-center.component';
import { LawComponent } from './features/law/law.component';
import { ContractComponent } from './features/contract/contract.component';
import { ModalSpecialtyComponent } from './features/specialty/components/modal-specialty/modal-specialty.component';
import { ModalInsertProfesionalComponent } from './features/professional/components/modal-insert-profesional/modal-insert-profesional.component';
import { ToastrModule } from 'ngx-toastr';
import { ModalInsertExtensionComponent } from './features/professional/components/modal-insert-extension/modal-insert-extension.component';
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { UserComponent } from './features/user/user.component';
import { FormExtensionComponent } from './features/professional/components/form-extension/form-extension.component';
import { UpdateFormActivityComponent } from './features/programming/components/update-form-activity/update-form-activity.component';
import { ExcelService } from './core/services/excel/excel.service';
import { AgGridModule } from 'ag-grid-angular';
import { BtnCellRenderer } from './features/law/btn-cell-renderer.component';
import { AuthGuard } from './guards/auth.guard';
import { ModalUserComponent } from './features/user/components/modal-user/modal-user.component';
import { DatePipe } from '@angular/common';
import { ModalModifyProfessionalComponent } from './features/professional/components/modal-modify-professional/modal-modify-professional.component';
import { distribucionHorasRenderer } from './features/law/distribucion_horas';
const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    HomeComponent,
    NavbarComponent,
    ProgrammingComponent,
    SynchronizerComponent,
    ModalProgrammingComponent,
    FormActivityComponent,
    SpecialtyComponent,
    ProfessionalComponent,
    ActivityComponent,
    CostCenterComponent,
    LawComponent,
    ContractComponent,
    ModalSpecialtyComponent,
    ModalInsertProfesionalComponent,
    ModalInsertExtensionComponent,
    UserComponent,
    FormExtensionComponent,
    ModalActivityComponent,
    ModalCostCenterComponent,
    ModalLawComponent,
    UpdateFormActivityComponent,
    BtnCellRenderer,
    distribucionHorasRenderer,
    ModalUserComponent,
    ModalModifyProfessionalComponent
  ],
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    Ng9RutModule,
    NgWizardModule.forRoot(ngWizardConfig),
    CommonModule,
    SidebarModule.forRoot(),
    ToastrModule.forRoot(),
    NgxPaginationModule,
    AgGridModule.withComponents([BtnCellRenderer,distribucionHorasRenderer])
  ],
  providers:  [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }, ExcelService, AuthGuard,DatePipe],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
