import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'fe-proplan';
  ngOnInit() {
    // this.loadScript("assets/vendor/jquery/dist/jquery.min.js").then(() => {console.log("Script Loaded");}).catch(() => {console.log("Script Problem");});
    // "node_modules/jquery/dist/jquery.min.js",
    // "src/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js",
    // "src/assets/vendor/js-cookie/js.cookie.js",
    // "src/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js",
    // "src/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js",
    // "src/assets/js/dashboard.js"
      this.loadScript("assets/vendor/jquery/dist/jquery.min.js");
      this.loadScript("assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js");
      this.loadScript("assets/vendor/js-cookie/js.cookie.js");
      this.loadScript("assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js");
      this.loadScript("assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js");
      this.loadScript("assets/js/dashboard.min.js");
  }
  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }
 }
