import { Component, OnInit } from '@angular/core';
import { ActivityService } from './../../core/services/activity/activity.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { BtnCellRenderer } from '../law/btn-cell-renderer.component';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
  providers: [ActivityService]
})
export class ActivityComponent implements OnInit {
  frameworkComponents: any;
  private gridApi;
  activities: any[] = [];
  activityTypes: any[] = [];
  activity: any = {id: '', descripcion: '', tipo_actividad: 1, rendimiento: false, horas_rendimiento: 0};
  maxPaginatorSize: number = 6;
  closeResult: any;
  pageSize: number = 6;
  page: number = 1;
  columnas = [
    {headerName: "codigo", field: "codigo", sortable: true, sort: 'asc',filter: 'agTextColumnFilter', },
    {headerName: "descripcion", field: "descripcion",  },
    {headerName: "tipo actividad", field: "tipo_actividad.descripcion",  },
    {
      headerName: 'Activo',
      cellRenderer: 'buttonRenderer', 
      cellRendererParams: {
      },
      resizable: true
    }
    
  ];
  gridColumnApi: any;

  constructor(private spinner:NgxSpinnerService, public activityService: ActivityService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.frameworkComponents = {
      buttonRenderer: BtnCellRenderer,
    }
    this.loadActivities();
    this.loadScript();
  }
  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }
  openModal(modalActivity, activity) {
    this.activity = {
      id: activity.id, 
      descripcion: activity.descripcion, 
      tipo_actividad: activity.tipo_actividad.id, 
      rendimiento: activity.rendimiento.exige, 
      horas_rendimiento: activity.rendimiento.horas,
      codigo: activity.codigo,
      activo: activity.activo
    };
    this.loadActivityTypes();
    this.modalService
    .open(modalActivity, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  closeModal(){
    this.modalService.dismissAll();
    this.loadActivities();
  }

  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }


  

  loadActivities(){
    this.spinner.show();
    this.activityService.getActivitiesCatalogue().subscribe((response: any) => {
      this.activities = response.actividades;
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

  loadActivityTypes(){
    this.activityService.getActivityTypesCatalogue().subscribe((response: any) => {
      this.activityTypes = response.tipos;
    }, (error) => {
      console.log(error);
    });
  }

  openModalAddActivity(modalActivity){
    this.activity = {id: '', descripcion: '', tipo_actividad: 1, rendimiento: false, horas_rendimiento: 0};
    this.loadActivityTypes();
    this.modalService
    .open(modalActivity, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  saveActivity(data){
    this.spinner.show();
    if(data.id == null || data.id == "") {
      this.activityService.addActivity(data).subscribe((response: any) => {
        this.spinner.hide();          
          Swal.fire(
            'Ingreso correcto!',
            'La actividad se ha ingresado correctamente',
            'success'
          )
          this.loadActivities();
      }, (error) => {
        this.spinner.hide();
      });
    } else {
      this.activityService.updateActivity(data, data.id).subscribe((response: any) => {
        if(!response.OK) {
        this.spinner.hide();
          Swal.fire(
            'Actualización no completada!',
            'La actividad no se ha actualizado correctamente',
            'error'
          )
        } else {          
          Swal.fire(
            'Actualizacion correcta!',
            'La actividad se ha actualizado correctamente',
            'success'
          )
          this.loadActivities();
        }
      }, (error) => {
        this.spinner.hide();
      });
    }
    this.closeModal();
  }
}
