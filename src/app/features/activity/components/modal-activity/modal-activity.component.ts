import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivityService } from 'src/app/core/services/activity/activity.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-modal-activity',
    templateUrl: './modal-activity.component.html'
  })
  export class ModalActivityComponent implements OnInit {

    @Input() activity: any;
    @Input() activityTypes: any = [];
    @Output() closeModals = new EventEmitter<string>();
    @Output() saveActivity = new EventEmitter<any>();
    public titleModal: string = '';
    public textButton: string = '';
    public inputCodigo: string = '';
    public inputDescripcion: string = '';
    public inputTipoActividad: number = 0;
    public inputRendimiento: boolean = false;
    public inputHorasRendimiento: number = 0;
    public formError: boolean = false;
    public formErrorMsg: string;
    maxHorasRendimiento = 12;
    isNew: boolean;
    constructor(private spinner:NgxSpinnerService,public activityService: ActivityService) { }

    ngOnInit(): void {
      if (this.activity.id == null || this.activity.id == '') {
        this.titleModal = 'Ingreso actividad'
        this.textButton = 'Ingresar actividad'
        this.isNew = true;
      } else {
        this.titleModal = 'Actualizar actividad ' + this.activity.descripcion;
        this.inputCodigo = this.activity.code;
        this.inputDescripcion = this.activity.descripcion;
        this.textButton = 'Actualizar actividad'
        this.isNew = false;
      }
    }

    closeModal() {
        this.closeModals.emit();
    }

    addActivity(){
      if (this.activity.codigo.trim() == '' || this.activity.codigo == null || this.activity.codigo == undefined) {
        this.formErrorshow('Ingrese un codigo por favor');
      } else if (this.activity.descripcion == '' || this.activity.descripcion == null || this.activity.descripcion == undefined) {
        this.formErrorshow('Ingrese una descripción por favor');
      } else if (this.activity.horas_rendimiento > this.maxHorasRendimiento) {
        this.formErrorshow('El maximo de horas de rendimiento es ' + this.maxHorasRendimiento);
      } else {
          this.saveActivity.emit(this.activity);
      }
    }

    formErrorshow(msg) {
      this.formErrorMsg = msg;
      this.formError = true;
    }
    inactiveActivity(id){
      Swal.fire({
        title: '¿Estás seguro de desactivar la actividad?',
        text: "Si la desactivas no se podra ver en la programación",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Desactivar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.activityService.deleteActivity(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Desactivado!',
              'La actividad ha sido desactivada correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en deleteActivity: ", error)
            this.spinner.hide();
          });
        }
      })
    }
  
    activeActivity(id){
      Swal.fire({
        title: '¿Estás seguro de activar la actividad?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Activar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.activityService.activeActivity(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Activado!',
              'La actividad ha sido activada correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en deleteActivity: ", error)
            this.spinner.hide();
          });
        }
      })
    }
  }