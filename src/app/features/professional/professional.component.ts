import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';

import { Professional } from './../../core/models/professional/professional';
import { ProfessionalService } from './../../core/services/professional/professional.service';
import { NgbdSortableHeader, SortEvent } from './../../core/directives/sortable.directive';
import { RutValidator } from 'ng9-rut';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ComunicationService } from './../../core/services/comunication/comunication.service';

@Component({
  selector: 'app-professional',
  templateUrl: './professional.component.html',
  styleUrls: ['./professional.component.scss']
})
export class ProfessionalComponent implements OnInit {
  public closeResult: any;
  public nameProfessional:string;
  public idProfessional:number;
  public typeActivity:string;
  public listenChangePeriods:any;
  public periodActual:number = 0;
  public porcentajesView:boolean = false;
  professionals$: Observable<Professional[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private modalService: NgbModal,
    public professionalService: ProfessionalService,
    public spinner:NgxSpinnerService,
    public comunicationService:ComunicationService) {
    this.professionals$ = professionalService.professional$;
    this.total$ = professionalService.total$;
    this.listenChangePeriods = this.comunicationService.listenChangePeriod().subscribe((idPeriodo: any) => {
        this.getProfessional(idPeriodo);
    },(error) => {
      (['comunicationService',error])
    });
  }

  ngOnInit(): void {
   
    this.loadScript();
    let periods =  JSON.parse(localStorage.getItem('Periods'));
    if(periods) {
      periods.forEach(element => {
          if(element.actual){this.periodActual = element.id}
      });
    }
    this.getProfessional(this.periodActual);
  }
  ngOnDestroy() {
    this.listenChangePeriods.unsubscribe();
  }

  onSort({column, direction}: SortEvent) {
   
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.professionalService.sortColumn = column;
    this.professionalService.sortDirection = direction;
  }
  getProfessional(idPeriod):void {
    this.porcentajesView = false;
    this.periodActual = idPeriod;
    this.spinner.show();
    this.professionalService.getProfessional(idPeriod, null).subscribe((response: any) => {
 
      this.spinner.hide();
      
    }, (error) => {
      this.spinner.hide();
    });
  }
  openModal(content, sizeModal, nameProfessional = null, idProfessional= null, typeActivity= null) {
    this.spinner.show();
    this.nameProfessional = nameProfessional;
    this.idProfessional = idProfessional;
    this.typeActivity = typeActivity;
    this.modalService
      .open(content, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: sizeModal })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeModal() {
    this.modalService.dismissAll();
  }
  refreshTable() {
    this.closeModal();
    this.professionalService.getProfessional(this.periodActual, null).subscribe((response: any) => {
      this.spinner.hide();
      Swal.fire(
        'Ingreso correcto',
        'El profesional ha sido ingresado correctamente',
        'success'
      )
    }, (error) => {
      this.spinner.hide();
    });
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);
  }
  getPorcentajes() {
    this.porcentajesView = false;
    this.professionalService.getProgrammingPorcentage(this.periodActual).subscribe((response: any) => {
      this.porcentajesView = true;
    }, (error) => {
      this.spinner.hide();
    });
  }
}


