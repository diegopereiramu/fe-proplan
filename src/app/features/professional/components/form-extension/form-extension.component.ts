import { Component, OnInit, Output, EventEmitter, Injectable, ViewEncapsulation, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import {
  NgbCalendar,
  NgbDateAdapter,
  NgbDateStruct,
  NgbDateParserFormatter,
  NgbDatepickerI18n
} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { LeyService } from './../../../../core/services/ley/ley.service';
import { CostCenterService } from './../../../../core/services/costCenter/cost-center.service';
import { ContractService } from './../../../../core/services/contract/contract.service';
import { ProfessionalService } from './../../../../core/services/professional/professional.service';
import { ToastrService } from 'ngx-toastr';

const I18N_VALUES = {
  'esp': {
    weekdays: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
    months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
  }
};

@Injectable()
export class I18n {
  language = 'esp';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private _i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}
@Component({
  selector: 'app-form-extension',
  templateUrl: './form-extension.component.html',
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }],
  styleUrls: ['./form-extension.component.scss']
})
export class FormExtensionComponent implements OnInit {
  @Input() idContrato:number;
  @Input() ley: string;
  @Output() seeDetails = new EventEmitter<string>();
  public inputHoras: string = '';
  public selectTurnos: any = 0;
  public selectObservaciones = 'Ninguno';
  public inputFeriados: string = '';
  public inputDescanso: string = '';
  public inputPermisos: string = '';
  public inputCongreso: string = '';
  public inputLactancia: string = '';
  public inputColacion: string = '';
  public txtHastaProgramacion;
  public txtDesdeProgramacion;
  public startDatesProgramacion;
  public endDatesProgramacion;
  public leyes: any = [];
  public ListCc = [];
  public selectedCC = [];
  disabledWeekHours = false;

  dropdownSettingsCc: IDropdownSettings = {};

  constructor(private costCenterService: CostCenterService,
    private spinner: NgxSpinnerService,
    private professionalService: ProfessionalService,
    private toastr: ToastrService, 
    private contractService:ContractService) {
    let d = new Date();
    let years = d.getFullYear();
    this.startDatesProgramacion = { day: 1, month: 1, year: years }
    this.txtDesdeProgramacion = { day: 1, month: 1, year: years }
    this.endDatesProgramacion = { day: 31, month: 12, year: years }
    this.txtHastaProgramacion = { day: 31, month: 12, year: years }
  }

  ngOnInit(): void {
    if(this.ley === '15076') {
      this.disabledWeekHours = true;
      this.inputHoras = '28';
    }
    this.dropdownSettingsCc = {
      singleSelection: false,
      idField: "id",
      textField: "descripcion",
      selectAllText: "Todos",
      unSelectAllText: "Todos",
      itemsShowLimit: 1,
      allowSearchFilter: false
    };
  }
  onItemSelectCc(item: any) {
  }
  onDeSelectCc(item: any) {
  }
  onSelectAllCc(items: any) {
  }
  onDeSelectAllCc(items: any) {
  }
  addExtension() {
    if (this.inputHoras.toString().trim() == '') {
      this.showWarning('Ingreses horas semanales contratadas');
    } else if (this.selectTurnos == 0) {
      this.showWarning('Seleccione sistema de turnos');
    } else if (this.inputFeriados.toString().trim() == '') {
      this.showWarning('Ingrese dias feriados legales');
    } else if (this.inputDescanso.toString().trim() == '') {
      this.showWarning('Ingrese dias de descanso');
    } else if (this.inputPermisos.toString().trim() == '') {
      this.showWarning('Ingrese dias permisos administrativos');
    } else if (this.inputCongreso.toString().trim() == '') {
      this.showWarning('Ingrese dias de congrreso y/o capacitación');
    } else if (this.inputLactancia.toString().trim() == '') {
      this.showWarning('Ingrese tiempo de lactancia');
    } else if (this.inputColacion.toString().trim() == '') {
      this.showWarning('Ingrese tiempo de colacion semanal');
    } else if (this.startDatesProgramacion.toString().trim() == '') {
      this.showWarning('Ingrese fecha inicio de programacion');
    } else if (this.endDatesProgramacion.toString().trim() == '') {
      this.showWarning('Ingrese fecha fin de programacion');
    } else {
      this.spinner.show();
      let cc = [];
      this.selectedCC.forEach(element => {
        cc.push(element.id)
      });
      let obj = {
        idContrato: this.idContrato,
        horas_semanales : this.inputHoras,
        feriados_legales: this.inputFeriados === undefined ? 0 : this.inputFeriados,
        descanso_compensatorio: this.inputDescanso === undefined ? 0 : this.inputDescanso,
        permiso_administrativo: this.inputPermisos === undefined ? 0 : this.inputPermisos,
        congreso_capacitacion: this.inputCongreso === undefined ? 0 : this.inputCongreso,
        lactancia_semanal: this.inputLactancia === undefined ? 0 : this.inputLactancia,
        colacion_semanal: this.inputColacion === undefined ? 0 : this.inputColacion,
        sistema_turno: this.selectTurnos == 'S' ? true : false,
        fecha_inicio :  this.txtDesdeProgramacion.year + '/' + this.txtDesdeProgramacion.month + '/' + this.txtDesdeProgramacion.day,
        fecha_termino:this.txtHastaProgramacion.year + '/' + this.txtHastaProgramacion.month + '/' + this.txtHastaProgramacion.day
      }
      this.contractService.creacionExtension(obj).subscribe((response: any) => {
        this.seeDetails.emit();
      }, (error) => {
        this.spinner.hide();
      });
    }
  }
  showWarning(msg) {
    this.toastr.error(msg, "Faltan datos", {
      positionClass: "toast-bottom-center"
    });
  }
}
