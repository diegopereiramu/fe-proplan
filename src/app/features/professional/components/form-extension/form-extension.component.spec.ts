import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormExtensionComponent } from './form-extension.component';

describe('FormExtensionComponent', () => {
  let component: FormExtensionComponent;
  let fixture: ComponentFixture<FormExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
