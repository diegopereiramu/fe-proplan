import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertProfesionalComponent } from './modal-insert-profesional.component';

describe('ModalInsertProfesionalComponent', () => {
  let component: ModalInsertProfesionalComponent;
  let fixture: ComponentFixture<ModalInsertProfesionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertProfesionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertProfesionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
