import { Component, OnInit, Output, EventEmitter, Injectable, ViewEncapsulation } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProfessionalService } from './../../../../core/services/professional/professional.service';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import {
  NgbCalendar,
  NgbDateAdapter,
  NgbDateStruct,
  NgbDateParserFormatter,
  NgbDatepickerI18n
} from '@ng-bootstrap/ng-bootstrap';
import { TituloService } from './../../../../core/services/titulo/titulo.service';
import { LeyService } from './../../../../core/services/ley/ley.service';
import { ToastrService } from 'ngx-toastr';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { CostCenterService } from './../../../../core/services/costCenter/cost-center.service';
import { DatePipe } from '@angular/common';

class Profesional {
  id: number;
  documento: string;
  tipo_documento_id: number;
  nombre: string;
  activo: boolean;
}
const I18N_VALUES = {
  'esp': {
    weekdays: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
    months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
  }
};

@Injectable()
export class I18n {
  language = 'esp';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private _i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-modal-insert-profesional',
  templateUrl: './modal-insert-profesional.component.html',
  styleUrls: ['./modal-insert-profesional.component.scss'],
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }],
  encapsulation: ViewEncapsulation.None
})
export class ModalInsertProfesionalComponent implements OnInit {
  @Output() closeModals = new EventEmitter<string>();
  @Output() refreshTable = new EventEmitter<string>();
  public listProfesional: Profesional[];
  public ListCc = [];
  public selectedCC: any = 0;
  dropdownSettingsCc: IDropdownSettings = {};
  public profesionalModel: any;
  public inputNombre: string;
  public idProfesional: number = 0;
  public tipoDocumento: number = 1;
  public selectTituloProfesional: number = 0;
  public selectLey: number = 0;
  public inputCodigoCorrelativo: number;
  public inputEspecialidad: string = '';
  public inputInicio: string = '';
  public inputFin: string = '';
  public inputHoras: string = '';
  public inputFeriados: string = '';
  public inputDescanso: string = '';
  public inputPermisos: string = '';
  public inputCongreso: string = '';
  public inputLactancia: string = '';
  public inputColacion: string = '';
  public selectTurnos: any = 0;
  public titulos: any = [];
  public leyes: any = [];
  public startDates;
  public txtDesde;
  public endDates;
  public txtHasta;
  public startDatesProgramacion;
  public txtDesdeProgramacion;
  public endDatesProgramacion;
  public txtHastaProgramacion;
  public selectObservaciones = 'Ninguno';
  model: NgbDateStruct;
  date: { year: number, month: number };
  select=true;

  constructor(private spinner: NgxSpinnerService,
    private professionalService: ProfessionalService,
    private tituloService: TituloService,
    private leyService: LeyService,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private costCenterService: CostCenterService) {
    let d = new Date();
    let years = d.getFullYear();
    this.startDates = new Date()
    this.startDates = { day: 1, month: 1, year: years }
    this.txtDesde = { day: 1, month: 1, year: years }
    this.endDates = { day: 31, month: 12, year: years }
    this.txtHasta = { day: 31, month: 12, year: years }
    this.startDatesProgramacion = { day: 1, month: 1, year: years }
    this.txtDesdeProgramacion = { day: 1, month: 1, year: years }
    this.endDatesProgramacion = { day: 31, month: 12, year: years }
    this.txtHastaProgramacion = { day: 31, month: 12, year: years }
      
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.listProfesional.filter(v => v.documento.indexOf(term) > -1).slice(0, 10))
    )

  formatter = (x: { documento: string }) => x.documento;

  ngOnInit(): void {
    this.getProfesionales();
    this.getTitulos();
    this.getLeyes();
    this.getCc();
    this.dropdownSettingsCc = {
      singleSelection: false,
      idField: "id",
      textField: "descripcion",
      selectAllText: "Todos",
      unSelectAllText: "Todos",
      itemsShowLimit: 1,
      allowSearchFilter: false
    };
  }

  closeModal() {
    this.closeModals.emit();
  }
  getProfesionales() {
    this.professionalService.getProfesional().subscribe((response: any) => {
      this.spinner.hide();
      if (response != undefined) {
        if (response.ok != false) {
          this.listProfesional = response.data;
        }
      }
    }, (error) => {
      this.spinner.hide();
    });
  }
  getRut(event) {
    this.idProfesional = event.item.id;
    this.inputNombre = event.item.nombre;
  }

  getTitulos() {
    this.tituloService.getTitulos().subscribe((response: any) => {
      this.titulos = response.titulos;
    }, (error) => {
      this.spinner.hide();
    });
  }
  getCc() {
    this.costCenterService.getCc().subscribe((response: any) => {
      this.ListCc = response.centro_costos.filter(cc => cc.activo);
    }, (error) => {
      this.spinner.hide();
    });

  }
  getLeyes() {
    this.leyService.getLey().subscribe((response: any) => {
      this.leyes = response.leyes;
    }, (error) => {
      this.spinner.hide();
    });
  }
  addProgramming() {
    console.log(this.datePipe.transform(new Date(this.txtHastaProgramacion.year + '/' + this.txtHastaProgramacion.month + '/' + this.txtHastaProgramacion.day),'yyyy-MM-dd'));
    const HastaProgramacion=this.datePipe.transform(new Date(this.txtHastaProgramacion.year + '/' + this.txtHastaProgramacion.month + '/' + this.txtHastaProgramacion.day),'yyyy-MM-dd');
    const DesdeProgramacion=this.datePipe.transform(new Date(this.txtDesdeProgramacion.year + '/' + this.txtDesdeProgramacion.month + '/' + this.txtDesdeProgramacion.day),'yyyy-MM-dd');
    const Desde=this.datePipe.transform(new Date( this.txtDesde.year + '/' + this.txtDesde.month + '/' + this.txtDesde.day),'yyyy-MM-dd');
    const Hasta=this.datePipe.transform(new Date( this.txtHasta.year + '/' + this.txtHasta.month + '/' + this.txtHasta.day),'yyyy-MM-dd');
    if (this.profesionalModel == undefined) {
      this.showWarning('Falta rut de profesional');
    } else if (this.inputNombre==undefined || this.inputNombre.trim() == '' ) {
      this.showWarning('Falta nombre de profesional');
    } else if (this.selectTituloProfesional==undefined || this.selectTituloProfesional == 0) {
      this.showWarning('Seleccione un titulo profesional');
    } else if (this.selectLey == 0) {
      this.showWarning('Seleccione una ley');
    } else if (this.inputCodigoCorrelativo==undefined || this.inputCodigoCorrelativo.toString().trim() == '') {
      this.showWarning('Ingrese codigo correlativo de contrato');
    } else if (this.startDates==undefined || this.startDates.toString().trim() == '') {
      this.showWarning('Ingrese fecha inicio de contrato');
    } else if (this.selectedCC == 0) {
      this.showWarning('Seleccione un centro de costo');  
    } else if (Hasta<Desde) {
      this.showWarning('fecha final de contrato debe ser mayor al inicio');
    } else if (this.inputHoras==undefined || this.inputHoras.toString().trim() == '') {
      this.showWarning('Ingreses horas semanales contratadas');
    } else if (this.selectTurnos == 0) {
      this.showWarning('Seleccione sistema de turnos');
    } else if (this.inputFeriados==undefined || this.inputFeriados.toString().trim() == '') {
      this.showWarning('Ingrese dias feriados legales');
    } else if (this.inputDescanso==undefined || this.inputDescanso.toString().trim() == '') {
      this.showWarning('Ingrese dias de descanso');
    } else if (this.inputPermisos==undefined || this.inputPermisos.toString().trim() == '') {
      this.showWarning('Ingrese dias permisos administrativos');
    } else if (this.inputCongreso==undefined || this.inputCongreso.toString().trim() == '') {
      this.showWarning('Ingrese dias de congrreso y/o capacitación');
    } else if (this.inputLactancia==undefined || this.inputLactancia.toString().trim() == '') {
      this.showWarning('Ingrese tiempo de lactancia');
    } else if (this.inputColacion==undefined || this.inputColacion.toString().trim() == '') {
      this.showWarning('Ingrese tiempo de colacion semanal');
    } else if (this.startDatesProgramacion==undefined || this.startDatesProgramacion.toString().trim() == '') {
      this.showWarning('Ingrese fecha inicio de programacion');
    } else if (this.endDatesProgramacion==undefined || this.endDatesProgramacion.toString().trim() == '') {
      this.showWarning('Ingrese fecha fin de programacion');
    } else if (HastaProgramacion<DesdeProgramacion) {
      this.showWarning('fecha final de programacion debe ser mayor al inicio');
    } else {
      this.spinner.show();
      let cc =this.selectedCC;
      let rutNombnres = this.profesionalModel.documento == undefined ? this.profesionalModel : this.profesionalModel.documento;
      let titulos = [];
      let obj = {
        nombre: this.inputNombre,
        documento: {
          numero: rutNombnres,
          tipo: 'RUT'
        },
        titulos: this.selectTituloProfesional,
        contratos: {
          ley: this.selectLey,
          codigo: this.inputCodigoCorrelativo,
          especialidad_sis: this.inputEspecialidad === '' || this.inputEspecialidad == '0' || this.inputEspecialidad == null || this.inputEspecialidad == undefined ? null : this.inputEspecialidad,
          horas_semanales: this.inputHoras,
          sistema_turno: this.selectTurnos == 'S' ? true : false,
          dias: {
            feriados_legales: this.inputFeriados === undefined ? 0 : this.inputFeriados,
            descanso_compensatorio: this.inputDescanso === undefined ? 0 : this.inputDescanso,
            permiso_administrativo: this.inputPermisos === undefined ? 0 : this.inputPermisos,
            congreso_capacitacion: this.inputCongreso === undefined ? 0 : this.inputCongreso,
          },
          tiempo: {
            lactancia_semanal: this.inputLactancia === undefined ? 0 : this.inputLactancia,
            colacion_semanal: this.inputColacion === undefined ? 0 : this.inputColacion
          },
          inicio: this.txtDesde.year + '/' + this.txtDesde.month + '/' + this.txtDesde.day,
          termino: this.txtHasta.year + '/' + this.txtHasta.month + '/' + this.txtHasta.day,
          centro_costos: cc
        },
        fechaInicioProgramacion: this.txtDesdeProgramacion.year + '/' + this.txtDesdeProgramacion.month + '/' + this.txtDesdeProgramacion.day,
        fechaFinProgramacion: this.txtHastaProgramacion.year + '/' + this.txtHastaProgramacion.month + '/' + this.txtHastaProgramacion.day
      }
      this.professionalService.creacionProfesional(obj).subscribe((response: any) => {
        this.refreshTable.emit();
      }, (error) => {
        this.spinner.hide();
      });
    }
  }

  
  showWarning(msg) {
    this.toastr.error(msg, "Faltan datos", {
      positionClass: "toast-bottom-center"
    });
  }
  onItemSelectCc(item: any) {
    
  }
  onDeSelectCc(item: any) {
  }
  onSelectAllCc(items: any) {
  }
  onDeSelectAllCc(items: any) {
  }
  leyChange(){
    if(this.selectLey==15076){
      this.inputHoras = ""+28;
    } else {
      this.inputHoras = "";
    }
  }
  onKeypressEvent(e: any){
    var regexValidKeys = (/[\d\-k]/i);
    var key = e.key || String.fromCharCode(e.keyCode);
        if (!regexValidKeys.test(key) && e.keyCode !== 9) {
          e.preventDefault();
          return false;
    }
  }
}
