import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalInsertExtensionComponent } from './modal-insert-extension.component';

describe('ModalInsertExtensionComponent', () => {
  let component: ModalInsertExtensionComponent;
  let fixture: ComponentFixture<ModalInsertExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInsertExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInsertExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
