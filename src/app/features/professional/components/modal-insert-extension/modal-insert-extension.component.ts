import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';
import {
  Component, QueryList, ViewChildren,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Contract } from './../../../../core/models/contract/contract';
import { ContractService } from './../../../../core/services/contract/contract.service';
import { ProgrammingService } from './../../../../core/services/programming/programming.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Programming } from './../../../../core/models/programming/programming';
import { NgbdSortableHeader, SortEvent } from './../../../../core/directives/sortable.directive';
import { Extension } from './../../../../core/models/contract/extension';
import { trigger, transition, style, animate, state } from '@angular/animations';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-insert-extension',
  templateUrl: './modal-insert-extension.component.html',
  styleUrls: ['./modal-insert-extension.component.scss'],
  animations: [
    trigger(
      'myAnimation',
      [
        transition(
          ':enter', [
          style({ opacity: 0, display: 'block' }),
          animate('0ms', style({ 'opacity': 1 }))
        ]
        ),
        transition(
          ':leave', [
          style({ 'opacity': 1, display: 'block' }),
          animate('0ms', style({ 'opacity': 0 })
          )])
      ])]
})
export class ModalInsertExtensionComponent implements OnInit {
  @Input() nameProfessional: string;
  @Input() idProfessional: number;
  @Input() typeActivity: string;
  @Input() periodActual: number;
  @Output() closeModals = new EventEmitter<string>();
  public contract: Contract[];
  public programming: Programming[];
  public idSelectContract: number = 0;
  public showProgramming: boolean;
  public titleModal: string = "";
  public showGeneralInformation: boolean = true;
  public showFormActivity: boolean = false;
  public hoursUsed: number = 0;
  public weeklyHours: number = 0;
  public cc: any = [];
  public isApprover: boolean = false;
  public isObserver: boolean = false;
  public isProgrammer: boolean = false;
  public numLaw: string = '';
  public idTypeActivity: number;
  public addedProgram: Programming[] = [];
  public extensions: Extension[];
  public idSelectExtension: number = 0;
  public feriadosLegales:number;
  public diasDescanso:number;
  public permisosAdministrativos:number;
  public capacitaciones:number;
  public lactancia:number;
  public colacion:number;
  programmings$: Observable<Programming[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    private contractService: ContractService,
    public spinner: NgxSpinnerService,
    public programmingService: ProgrammingService) {
    this.programmings$ = programmingService.programming$;
    this.total$ = programmingService.total$;
  }

  ngOnInit(): void {
    this.idTypeActivity = this.typeActivity == "M" ? 1 : 2;
    this.titleModal = 'Contratos ' + this.nameProfessional;
    this.getContratos();
    let dataUser = JSON.parse(localStorage.getItem('Personal-data'));
    dataUser.rols.forEach(element => {
      if (element.id == 5 || element.id == 1) {
        this.isApprover = true;
      } else if (element.id == 4) {
        this.isObserver = true;
      } else {
        this.isProgrammer = true;
      }
    });
  }
  closeModal() {
    this.closeModals.emit();
  }
  getContratos() {
    this.contractService.getContractByIdProfessional(this.idProfessional, this.periodActual).subscribe((response: any) => {
      if (response.data.length > 0) {
        this.contract = response.data;
        this.idSelectContract = this.contract[0].id;
        this.changeContract('event');
        this.spinner.hide();
      }
    }, (error) => {
      
    });
  }
  changeContract(event) {
    this.extensions = [];
    if (this.idSelectContract != 0) {
      let extension: any = this.contract.find(c => c.id == this.idSelectContract).extensiones;
      extension.forEach(element => {
        this.extensions.push(element);
        if(this.extensions.length != 0) {
          this.showProgramming = true;
        } else {
          this.showProgramming = false;
        }
        this.numLaw = this.contract.find(c => c.id == this.idSelectContract).ley.codigo;
    
      });
    }
  }
  changeExtension(event) {
    this.hoursUsed = 0;
    if (this.idSelectExtension != 0) {
      this.weeklyHours = this.extensions.find(c => c.id == this.idSelectExtension).horas_semanales;
      this.feriadosLegales = this.extensions.find(c => c.id == this.idSelectExtension).dias.feriados_legales;
      this.diasDescanso =  this.extensions.find(c => c.id == this.idSelectExtension).dias.descanco_compensatorio;
      this.permisosAdministrativos =  this.extensions.find(c => c.id == this.idSelectExtension).dias.permiso_administrativo;
      this.capacitaciones =  this.extensions.find(c => c.id == this.idSelectExtension).dias.congreso_capacitacion;
      this.lactancia =  this.extensions.find(c => c.id == this.idSelectExtension).tiempo.lactancia_semanal;
      this.colacion = this.extensions.find(c => c.id == this.idSelectExtension).tiempo.colacion_semanal;
      this.cc = this.contract.find(c => c.id == this.idSelectContract).centro_costos;
      this.numLaw = this.contract.find(c => c.id == this.idSelectContract).ley.codigo;
      this.spinner.show();
      this.programmingService.getProgrammingByContractExtension(this.idSelectExtension).subscribe((response: any) => {
        this.addedProgram = response;
        response.forEach(activity => {
          if (activity.aprobado == true || activity.aprobado == null) {
            this.hoursUsed = this.hoursUsed + activity.horas.asignadas;
          }
        });
        if (response.length > 0) {
          this.showProgramming = true
        } else {
          this.showProgramming = false
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
      });
    }
  }
  onSort({ column, direction }: SortEvent) {
   
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.programmingService.sortColumn = column;
    this.programmingService.sortDirection = direction;
  }
  addActivity() {
    this.showGeneralInformation = !this.showGeneralInformation;
    this.showFormActivity = !this.showFormActivity;

  }
  backActivity() {
    this.showGeneralInformation = !this.showGeneralInformation;
    this.showFormActivity = !this.showFormActivity;
  }
  seeDetails() {
    this.getContratos();
    this.showGeneralInformation = !this.showGeneralInformation;
    this.showFormActivity = !this.showFormActivity;
  }
  editActivity() {

  }
  deleteActivity(idProgramming) {
    Swal.fire({
      title: '¿Estás seguro de eliminar la programación?',
      text: "Si lo eliminas tendras que volver a ingresarlo nuevamente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.programmingService.deleteProgramming(idProgramming).subscribe((response: any) => {
          this.changeContract('event');
          Swal.fire(
            'Eliminado!',
            'La programación ha sido eliminada correctamente',
            'success'
          )
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }
  reviewChange(status, idProgramming) {    
    let title = status == true ? "¿Estás seguro de aprobar esta programación?" : "¿Estás seguro de rechazar esta programación?";
    let text = status == true ? "¿Si apruebas no volveras a poder cambiar la programación?" : "¿Si rechazas no volveras a poder cambiar la programación?";
    let button = status == true ? "Aprobar" : "Rechazar";

    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, ' + button,
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.programmingService.changeStatus(idProgramming, status).subscribe((response: any) => {
          this.changeContract('event');
          Swal.fire(
            '¡Cambio con éxito!',
            'La programación ha sido cambiada correctamente',
            'success'
          )
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }
}
