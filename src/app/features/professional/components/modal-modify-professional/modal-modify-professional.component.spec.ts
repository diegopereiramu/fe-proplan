import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModifyProfessionalComponent } from './modal-modify-professional.component';

describe('ModalModifyProfessionalComponent', () => {
  let component: ModalModifyProfessionalComponent;
  let fixture: ComponentFixture<ModalModifyProfessionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalModifyProfessionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalModifyProfessionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
