import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ComunicationService } from 'src/app/core/services/comunication/comunication.service';
import { LeyService } from 'src/app/core/services/ley/ley.service';
import { ProgrammingService } from 'src/app/core/services/programming/programming.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  leyes = [];
  idPeriodo;
  selectLey;
  horasContratadas;
  horasProgramadas;
  horasDisponibles;
  public listenChangePeriods: any;
  
  constructor(private programmingService: ProgrammingService, private leyService: LeyService, private spinner:NgxSpinnerService,
    public comunicationService: ComunicationService) { this.listenChangePeriods = this.comunicationService.listenChangePeriod().subscribe((idPeriodo: any) => {
      this.datos();
    }, (error) => {
      (['comunicationService', error])
    }); }

  ngOnInit(): void {
    this.loadScript();
   this.datos();
  }
  public datos()
  {
    this.leyes=[];
    this.leyService.getLey().subscribe((response: any) => {
      response.leyes.forEach(ley => {
        if(ley.activo){
          this.leyes.push(ley);
        }
      });
      if(this.leyes.length>0){
        this.selectLey = this.leyes[0].codigo;
        this.updateDashboard();
      }
    });
    let periods = JSON.parse(localStorage.getItem('Periods'));
    if (periods) {
      periods.forEach(element => {
        if (element.actual) { this.idPeriodo = element.id }
      });
    }
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }

  updateDashboard() {
    //TODO: crear nuevo ms para reportes
    const request = {
      ley: this.selectLey,
      periodo: this.idPeriodo
    }
    this.programmingService.getResumenLey(request).subscribe((response: any) => {
      this.horasContratadas = response.horas_contratadas;
      this.horasProgramadas = response.horas_programadas;
      this.horasDisponibles = response.horas_contratadas - response.horas_programadas;
    });
  }
}
