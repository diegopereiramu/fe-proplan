import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';
import {
  Component, QueryList, ViewChildren,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Contract } from './../../../../core/models/contract/contract';
import { ContractService } from './../../../../core/services/contract/contract.service';
import { ProgrammingService } from './../../../../core/services/programming/programming.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Programming } from './../../../../core/models/programming/programming';
import { NgbdSortableHeader, SortEvent } from './../../../../core/directives/sortable.directive';
import { Extension } from './../../../../core/models/contract/extension';
import { trigger, transition, style, animate, state } from '@angular/animations';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { HelpersService } from './../../../../core/services/helpers/helpers.service';
import { ActivityService } from 'src/app/core/services/activity/activity.service';
@Component({
  selector: 'app-modal-programming',
  templateUrl: './modal-programming.component.html',
  styleUrls: ['./modal-programming.component.scss'],
  providers: [ProgrammingService, DecimalPipe],
  animations: [
    trigger(
      'myAnimation',
      [
        transition(
          ':enter', [
          style({ opacity: 0, display: 'block' }),
          animate('0ms', style({ 'opacity': 1 }))
        ]
        ),
        transition(
          ':leave', [
          style({ 'opacity': 1, display: 'block' }),
          animate('0ms', style({ 'opacity': 0 })
          )])
      ])]
})
export class ModalProgrammingComponent implements OnInit {
  @Input() nameProfessional: string;
  @Input() idProfessional: number;
  @Input() typeActivity: string;
  @Input() periodActual: number;
  @Output() closeModals = new EventEmitter<string>();
  public contract: Contract[];
  public programming: Programming[];
  public idSelectContract: number = 0;
  public showProgramming: boolean;
  public titleModal: string = "";
  public showGeneralInformation: boolean = true;
  public showFormActivity: boolean = false;
  public showUpdateFormActivity: boolean = false;
  public hoursUsed: number = 0;
  public weeklyHours: number = 0;
  public cc: any = [];
  public isApprover: boolean = false;
  public isObserver: boolean = false;
  public isProgrammer: boolean = false;
  public numLaw: string = '';
  public idTypeActivity: number;
  public addedProgram: Programming[] = [];
  public extensions: Extension[];
  public idSelectExtension: number = 0;
  public feriadosLegales: number;
  public diasDescanso: number;
  public permisosAdministrativos: number;
  public capacitaciones: number;
  public lactancia: number;
  public colacion: number;
  public fechaInicio: string;
  public fechaFin: string;
  public totalHorasContradas = 0;
  public feriadosLegalesGOB: any = [];
  public numberDiasDescuentos: number = 0;
  public diasHabilesAnual: number = 0;
  programmings$: Observable<Programming[]>;
  total$: Observable<number>;
  activity;
  activityTypes;
  closeResult;
  programmingActivityToEdit;
  

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    private contractService: ContractService,
    public spinner: NgxSpinnerService,
    public programmingService: ProgrammingService,
    public helpersService:HelpersService,
    public activityService: ActivityService) {
    this.programmings$ = programmingService.programming$;
    this.total$ = programmingService.total$;
  }

  ngOnInit(): void {
    this.idTypeActivity = this.typeActivity == "M" ? 1 : 2;
    this.titleModal = 'Contratos ' + this.nameProfessional;
    this.getContratos();
    this.getFeriados();
    let dataUser = JSON.parse(localStorage.getItem('Personal-data'));
    dataUser.rols.forEach(element => {
      if (element.id == 5) {
        this.isApprover = true;
      } else if (element.id == 4) {
        this.isObserver = true;
      } else if (element.id == 2){
        this.isProgrammer = true;
      } else if (element.id == 1){
        this.isProgrammer = true;
      }
    });
  }
  closeModal() {
    this.closeModals.emit();
  }
  getFeriados() {
    let fecha = new Date();
    let anno = fecha.getFullYear();
    this.programmingService.getDiasFeriado(anno).subscribe((response: any) => {
      this.feriadosLegalesGOB = response;
      this.diasHabilesAnual = this.helpersService.getDays(moment('01/01/'+anno, "DD-MM-YYYY"), moment('31/12/'+anno, "DD-MM-YYYY"), this.feriadosLegalesGOB);
    }, (error) => {
      alert('Problemas en el api de feriados')
      this.spinner.hide();
    });
  }
  getContratos() {
    this.contractService.getContractByIdProfessional(this.idProfessional, this.periodActual).subscribe((response: any) => {
      if (response.data.length > 0) {
        this.contract = response.data;
        let totalHorasContratadas = 0;
        response.data.forEach(contratos => {
          if (contratos.activo) {
            contratos.extensiones.forEach(extensiones => {
              if (extensiones.activo) {
                totalHorasContratadas = totalHorasContratadas + parseFloat(extensiones.horas_semanales);
              }
            });
          }
        });
        this.totalHorasContradas = totalHorasContratadas;
        this.titleModal = 'Contratos ' + this.nameProfessional + ' - Horas total contratadas: ' + this.totalHorasContradas;
        this.spinner.hide();
      }
    }, (error) => {
      // this.spinner.hide();
    });
  }
  changeContract(event) {
    this.extensions = [];
    if (this.idSelectContract != 0) {
      let extension: any = this.contract.find(c => c.id == this.idSelectContract).extensiones;
      extension.forEach(element => {
        if (element.activo) {
          //SOLO MOSTRAR EXTENSION ACTIVA
          this.extensions.push(element);
          this.idSelectExtension = element.id;
          this.changeExtension('event');
        }
      });
    }
  }

  round(num, decimal){
    return num.toFixed(decimal).replace('.', ',');
    //return num;
  }

  changeExtension(event) {
    
    this.hoursUsed = 0;
    if (this.idSelectExtension != 0) {
      this.weeklyHours = this.extensions.find(c => c.id == this.idSelectExtension).horas_semanales;
      this.feriadosLegales = this.extensions.find(c => c.id == this.idSelectExtension).dias.feriados_legales;
      this.diasDescanso = this.extensions.find(c => c.id == this.idSelectExtension).dias.descanco_compensatorio;
      this.permisosAdministrativos = this.extensions.find(c => c.id == this.idSelectExtension).dias.permiso_administrativo;
      this.capacitaciones = this.extensions.find(c => c.id == this.idSelectExtension).dias.congreso_capacitacion;
      this.lactancia = this.extensions.find(c => c.id == this.idSelectExtension).tiempo.lactancia_semanal;
      this.colacion = this.extensions.find(c => c.id == this.idSelectExtension).tiempo.colacion_semanal;
      this.cc = this.contract.find(c => c.id == this.idSelectContract).centro_costos;
      this.numLaw = this.contract.find(c => c.id == this.idSelectContract).ley.codigo;
      this.fechaInicio = this.extensions.find(c => c.id == this.idSelectExtension).fecha_inicio;
      this.fechaFin = this.extensions.find(c => c.id == this.idSelectExtension).fecha_termino;
      this.spinner.show();
      this.programmingService.getProgrammingByContractExtension(this.idSelectExtension).subscribe((response: any) => {
        this.numberDiasDescuentos = this.helpersService.getDays(moment(this.fechaInicio, "DD-MM-YYYY"), moment(this.fechaFin, "DD-MM-YYYY"), this.feriadosLegalesGOB);
        this.addedProgram = response;
        response.forEach(activity => {
          if (activity.aprobado == true || activity.aprobado == null) {
            this.hoursUsed = this.hoursUsed + parseFloat(activity.horas.asignadas);
          }
          activity.horas.legales_descuento = this.descuentosLegales(activity.horas.asignadas);
        });
        if (response.length > 0) {
          this.showProgramming = true
        } else {
          this.showProgramming = false
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
      });
    }
  }
  descuentosLegales(asignadas) {
    let number = this.numberDiasDescuentos;

    let descuentosLegales = ((this.feriadosLegales + this.diasDescanso + this.permisosAdministrativos + this.capacitaciones) / 5 * asignadas + (number - (this.feriadosLegales + this.diasDescanso + this.permisosAdministrativos + this.capacitaciones)) / 5 * (this.lactancia + this.colacion) / 60 * asignadas / this.totalHorasContradas).toFixed(1).toString();
    return parseFloat(descuentosLegales);
  }
  porcentajeProgramado(horasAsignadas) {
    return this.round(horasAsignadas/this.weeklyHours * 100, 2);
  }
  calcularProduccion() {
    
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.programmingService.sortColumn = column;
    this.programmingService.sortDirection = direction;
  }
  addActivity() {
    this.showGeneralInformation = !this.showGeneralInformation;
    this.showFormActivity = !this.showFormActivity;
    // this.spinner.show();
  }
  backActivity() {
    this.showGeneralInformation = true;
    this.showFormActivity = false;  
    this.showUpdateFormActivity = false;
  }
  seeDetails() {
    this.changeContract('listar');
    this.showGeneralInformation = true;
    this.showFormActivity = false;
    this.showUpdateFormActivity = false;
  }
  
  editActivity(programmingActivity) {
    this.showGeneralInformation = false;
    this.showFormActivity = false;
    this.showUpdateFormActivity = true;
    this.programmingActivityToEdit = programmingActivity;
  }

  deleteActivity(idProgramming) {
    Swal.fire({
      title: '¿Estás seguro de eliminar la programación?',
      text: "Si lo eliminas tendras que volver a ingresarlo nuevamente",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.programmingService.deleteProgramming(idProgramming).subscribe((response: any) => {
          this.changeContract('event');
          Swal.fire(
            'Eliminado!',
            'La programación ha sido eliminada correctamente',
            'success'
          )
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }
  reviewChange(status, idProgramming) {
    let title = status == true ? "¿Estás seguro de aprobar esta programación?" : "¿Estás seguro de rechazar esta programación?";
    let text = status == true ? "¿Si apruebas no volveras a poder cambiar la programación?" : "¿Si rechazas no volveras a poder cambiar la programación?";
    let button = status == true ? "Aprobar" : "Rechazar";

    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, ' + button,
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.programmingService.changeStatus(idProgramming, status).subscribe((response: any) => {
          this.changeContract('event');
          Swal.fire(
            '¡Cambio con éxito!',
            'La programación ha sido cambiada correctamente',
            'success'
          )
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }
  daysOfYear(){
    return this.helpersService.daysOfYear((new Date()).getFullYear());
  }
  weeksOfYear(){
    return this.daysOfYear()/7;
  }

  calculateProduction(asignadas, rendimiento){
    let lactanciaAnual = this.lactancia/60 * this.weeksOfYear()/24;
    let colacionAnual = this.colacion/60 * this.weeksOfYear()/24;
    let semanasHabilesPersona = (this.daysOfYear() - this.feriadosLegales - this.permisosAdministrativos - this.capacitaciones - this.diasDescanso - lactanciaAnual - colacionAnual)/7;
   let multiplicadorExcel = this.daysOfYear();
    let horasLegalesDescuentoSegunExcel =  ((this.feriadosLegales + this.diasDescanso + this.permisosAdministrativos + this.capacitaciones)/5*asignadas
    +(250.5-(this.feriadosLegales + this.diasDescanso + this.permisosAdministrativos + this.capacitaciones))/5
    *(this.lactancia+this.colacion)/60*asignadas/this.weeklyHours);
    return (asignadas * 50.1 - horasLegalesDescuentoSegunExcel) * rendimiento;

  }
  logProduction(a,r) {
    let lactanciaAnual = this.lactancia/60 * this.weeksOfYear()/24;
    let colacionAnual = this.colacion/60 * this.weeksOfYear()/24;
    let semanasHabilesPersona = (this.daysOfYear() - this.feriadosLegales - this.permisosAdministrativos - this.capacitaciones - this.diasDescanso - lactanciaAnual - colacionAnual)/7;
  }
} 
