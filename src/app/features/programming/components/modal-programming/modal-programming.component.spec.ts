import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalProgrammingComponent } from './modal-programming.component';

describe('ModalProgrammingComponent', () => {
  let component: ModalProgrammingComponent;
  let fixture: ComponentFixture<ModalProgrammingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalProgrammingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProgrammingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
