import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Activity } from 'src/app/core/models/activity/activity';
import { Programming } from 'src/app/core/models/programming/programming';
import { Specialty } from 'src/app/core/models/specialty/specialty';
import { ProgrammingService } from 'src/app/core/services/programming/programming.service';

@Component({
  selector: 'app-update-form-activity',
  templateUrl: './update-form-activity.component.html',
  styleUrls: ['./update-form-activity.component.scss']
})
export class UpdateFormActivityComponent implements OnInit {
  @Input() programmingActivity: any;
  @Output() seeDetails = new EventEmitter<string>();
  @Input() weeklyHours: number;
  @Input() numLaw: string;
  @Input() hoursUsed: number;
  inputHorasIngresadas: number;
  inputRendmiento: number;
  idSelectCC: number = 0;
  formError:boolean;
  formErrorMsg:string;
  maxHorasRendimiento = 12;
  descuentosLegales: any;
  numberDiasDescuentos: any;
  feriadosLegales: any;
  diasDescanso: any;
  permisosAdministrativos: any;
  capacitaciones: any;
  lactancia: any;
  colacion: any;
  totalHorasContradas: number;
  porcentajeActividad: string;
  disabledProgramming: boolean = false;
  horasIniciales: number;
  

  constructor(
    private programmingService: ProgrammingService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.horasIniciales = this.inputHorasIngresadas;
    this.inputHorasIngresadas = this.programmingActivity.horas.asignadas;
    this.inputRendmiento = this.programmingActivity.horas.rendimiento;
    if(this.numLaw == '15076') {
      this.disabledProgramming = true;
    }
  }

  updateActividad() {
    if(this.inputHorasIngresadas == undefined || this.inputHorasIngresadas == null || this.inputHorasIngresadas == 0) {
     this.formErrorshow("Falta ingresar horas ingresadas");
    } else if (this.inputHorasIngresadas + this.hoursUsed - this.horasIniciales > this.weeklyHours) {
      this.formErrorshow("Las horas ingresadas exceden las horas disponibles");
    } else if (this.inputHorasIngresadas > this.weeklyHours){
      this.formErrorshow("Las horas ingresadas exceden las horas semanales");
    } else if(this.inputRendmiento == undefined || this.inputRendmiento == null) {
      this.formErrorshow("Falta ingresar rendimiento");
    } else if(this.inputRendmiento > this.maxHorasRendimiento) {
      this.formErrorshow('El maximo de horas de rendimiento es ' + this.maxHorasRendimiento);
    } else {
      this.spinner.show();
      let request = {
        id: this.programmingActivity.id,
        horas: {
          asignadas: this.inputHorasIngresadas,
          rendimiento: this.inputRendmiento,
          legales_descuento: this.descuentosLegales
        }
      }
      this.programmingService.updateActivityInContract(request).subscribe((response: any) => {
        this.spinner.hide();
        this.seeDetails.emit();
      }, (error) => {
        this.spinner.hide();
      });
    }
  }

  preventInput(event) {
    let value=this.inputHorasIngresadas;
    let maxValue = this.weeklyHours - this.hoursUsed;
    if (value >= maxValue){
      event.preventDefault()
      this.inputHorasIngresadas = maxValue;
    }
  }

  changeHours() {
    let number = this.numberDiasDescuentos;
    this.descuentosLegales = ((this.feriadosLegales+this.diasDescanso+this.permisosAdministrativos+this.capacitaciones)/5*this.inputHorasIngresadas+(number-(this.feriadosLegales+this.diasDescanso+this.permisosAdministrativos+this.capacitaciones))/5*(this.lactancia+this.colacion)/60*this.inputHorasIngresadas/this.totalHorasContradas).toFixed(1).toString();
    this.porcentajeActividad = ((this.inputHorasIngresadas * 100)/this.weeklyHours).toFixed(2).toString() + '%';
  }

  formErrorshow(msg) {
    this.formErrorMsg = msg;
    this.formError = true;
  }

}
