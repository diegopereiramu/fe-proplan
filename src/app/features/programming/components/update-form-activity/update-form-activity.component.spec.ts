import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFormActivityComponent } from './update-form-activity.component';

describe('UpdateFormActivityComponent', () => {
  let component: UpdateFormActivityComponent;
  let fixture: ComponentFixture<UpdateFormActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFormActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFormActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
