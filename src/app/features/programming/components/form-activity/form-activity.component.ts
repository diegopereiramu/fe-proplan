import { DecimalPipe } from '@angular/common';
import {
  Component, QueryList, ViewChildren,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { SpecialtyService } from './../../../../core/services/specialty/specialty.service';
import { Specialty } from './../../../../core/models/specialty/specialty';
import { ActivityService } from './../../../../core/services/activity/activity.service';
import { ProgrammingService } from './../../../../core/services/programming/programming.service';
import { Programming } from './../../../../core/models/programming/programming';
import { Activity } from './../../../../core/models/activity/activity';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-form-activity',
  templateUrl: './form-activity.component.html',
  styleUrls: ['./form-activity.component.scss'],
})
export class FormActivityComponent implements OnInit {
  @Input() weeklyHours: number;
  @Input() hoursUsed: number;
  @Input() idTypeActivity: number;
  @Input() idSelectExtension: number;
  @Input() periodActual:number;
  @Input() numLaw:string;
  @Input() cc: any;
  @Input() addedProgram:Programming[] = [];
  @Input() feriadosLegales:number;
  @Input() diasDescanso:number;
  @Input() permisosAdministrativos:number;
  @Input() capacitaciones:number;
  @Input() lactancia:number;
  @Input() colacion:number;
  @Input() totalHorasContradas:number;
  @Input() numberDiasDescuentos:number;
  @Output() seeDetails = new EventEmitter<string>();
  public specialty: Specialty[];
  public activity: Activity[];
  public specialtyModel: any;
  public activityModel: any;
  public inputHorasIngresadas: number;
  public inputRendmiento: number;
  public descuentosLegales: string = '-';
  public idSelectCC: number = 0;
  public activitySelected: Activity;
  public specialtySelected:number = 0;
  public formError:boolean;
  public formErrorMsg:string;
  public disabledProgramming:boolean = false;
  public showActivity:boolean = false;
  public showSpecialty:boolean = true;
  public porcentajeActividad:string = '-';
  public produccionAnual:string = '-';
  maxHorasRendimiento = 12;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.specialty.filter(v => v.descripcion.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatter = (x: { descripcion: string }) => x.descripcion;

  searchActivity = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.activity.filter(v => v.descripcion.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  formatterActivity = (x: { descripcion: string }) => x.descripcion;
  constructor(private specialtyService: SpecialtyService,
    private spinner: NgxSpinnerService,
    private activityService: ActivityService,
    private programmingService:ProgrammingService) { }

  ngOnInit(): void {
    if(this.numLaw == '15076') {
      this.disabledProgramming = true;
      this.inputHorasIngresadas = this.weeklyHours;
      this.changeHours();
    }
  }
  getSpecialty() {
    this.specialtyService.getSpecialtyActive().subscribe((response: any) => {
      this.specialty = response.especialidades;
      this.spinner.hide();
    }, (error) => {
    });
  }
  getActivityBySpecialty(event) {
    this.showActivity = true;
    this.spinner.show();
    this.specialtySelected = event.item.id
    this.activityService.getActivityBySpecialty(this.specialtySelected, 1).subscribe((response: any) => {
      this.activity = response.actividades;
      this.addedProgram.forEach(activities => {
        if(activities.centro_costo.id == this.idSelectCC) {
          if(activities.especialidad.id ==  this.specialtySelected) {
            this.activity.forEach(actity => {
                if (actity.id == activities.actividad.id) {
                  this.activity.splice(this.activity.indexOf(actity), 1)
                }
              });
          }
        }
      });
      this.spinner.hide();
    }, (error) => {
    });
  }
  getActivity() {
    this.activityService.getActivityBySpecialty(null, 2).subscribe((response: any) => {
      this.activity = response.actividades;
      this.addedProgram.forEach(activities => {
        if(activities.centro_costo.id == this.idSelectCC) {
          this.activity.forEach(actity => {
            if (actity.id == activities.actividad.id) {
              this.activity.splice(this.activity.indexOf(actity), 1)
            }
          });
        }
      });
      this.spinner.hide();
    }, (error) => {
    });
  }
  filterActivity(event) {
    
    this.activitySelected = event;
    this.inputRendmiento = this.activitySelected.rendimiento.horas;
  }
  changeCC(event) {
    this.spinner.show();
    if(this.idTypeActivity == 1) {
      this.getSpecialty();
    } else {
      this.getActivity();
      this.showActivity = true;
      this.showSpecialty = false;
    }
  }
  formErrorshow(msg) {
    this.formErrorMsg = msg;
    this.formError = true;
  }
  changeHours() {
    this.descuentosLegales = (
      (this.feriadosLegales+this.diasDescanso+this.permisosAdministrativos+this.capacitaciones)/5
      *
      this.inputHorasIngresadas
      +
      (250.5-(this.feriadosLegales+this.diasDescanso+this.permisosAdministrativos+this.capacitaciones))/5
      *
      (this.lactancia+this.colacion)/60*this.inputHorasIngresadas/this.totalHorasContradas).toFixed(1).toString();
    
    
      this.porcentajeActividad = ((this.inputHorasIngresadas * 100)/this.weeklyHours).toFixed(2).toString() + '%';
  }
  addActividad() {
    if(this.idTypeActivity == 2) {this.specialtySelected = null}
    if(this.inputHorasIngresadas == undefined || this.inputHorasIngresadas == null || this.inputHorasIngresadas == 0) {
     this.formErrorshow("Falta ingresar horas ingresadas");
    } else if (this.inputHorasIngresadas + this.hoursUsed > this.weeklyHours) {
      this.formErrorshow("Las horas ingresadas exceden las horas disponibles");
    } else if(this.inputHorasIngresadas > this.weeklyHours) {
        this.formErrorshow("Las horas ingresadas exceden las horas semanales")
    } else if(this.idSelectCC == 0) {
     this.formErrorshow("Falta seleccionar un centro de costo");
    } else if(this.inputRendmiento == undefined || this.inputRendmiento == null) {
      this.formErrorshow("Falta ingresar rendimiento");
    } else if(this.descuentosLegales == undefined || this.descuentosLegales == null) {
      this.formErrorshow("Falta ingresar descuentos legales")
    } else if(this.inputRendmiento > this.maxHorasRendimiento) {
      this.formErrorshow('El maximo de horas de rendimiento es ' + this.maxHorasRendimiento);
    } else {
      this.spinner.show();
      let request = {
        extensionContrato: this.idSelectExtension,
        actividad: this.activitySelected.id,
        periodo: this.periodActual,
        especialidad: this.specialtySelected,
        centro_costo: this.idSelectCC,
        horas: {
          asignadas: this.inputHorasIngresadas,
          rendimiento: this.inputRendmiento,
          legales_descuento: this.descuentosLegales
        }
      }
      this.programmingService.addActivityToContract(request).subscribe((response: any) => {
        this.seeDetails.emit();
        this.spinner.hide();
      }, (error) => {
      });
    }
  }
  preventInput(event){
    let value=this.inputHorasIngresadas;
    let maxValue = this.weeklyHours - this.hoursUsed;
    if (value >= maxValue){
      event.preventDefault()
      this.inputHorasIngresadas = maxValue;
    }
  }

}


