import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';

import { Professional } from './../../core/models/professional/professional';
import { ProfessionalService } from './../../core/services/professional/professional.service';
import { NgbdSortableHeader, SortEvent } from './../../core/directives/sortable.directive';
import { RutValidator } from 'ng9-rut';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ComunicationService } from './../../core/services/comunication/comunication.service';

@Component({
  selector: 'app-programming',
  templateUrl: './programming.component.html',
  styleUrls: ['./programming.component.scss'],
  providers: [ProfessionalService]
})
export class ProgrammingComponent implements OnInit {
  public closeResult: any;
  public nameProfessional: string;
  public idProfessional: number;
  public typeActivity: string;
  public listenChangePeriods: any;
  public periodActual: number = 0;
  public porcentajesView: boolean = false;
  professionals$: Observable<Professional[]>;
  total$: Observable<number>;
  hasAccess = false;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private modalService: NgbModal,
    public professionalService: ProfessionalService,
    public spinner: NgxSpinnerService,
    public comunicationService: ComunicationService) {
    this.professionals$ = professionalService.professional$;
    this.total$ = professionalService.total$;
    this.listenChangePeriods = this.comunicationService.listenChangePeriod().subscribe((idPeriodo: any) => {
      this.getProfessional(idPeriodo);
    }, (error) => {
      (['comunicationService', error])
    });
  }

  ngOnInit(): void {
    // this.spinner.show("table");
    this.loadScript();
    let periods = JSON.parse(localStorage.getItem('Periods'));
    if (periods) {
      periods.forEach(element => {
        if (element.actual) { this.periodActual = element.id }
      });
    }
    this.getProfessional(this.periodActual);
    if (localStorage.getItem('Personal-data')){
      const personalData = JSON.parse(localStorage.getItem('Personal-data'));
      personalData.rols.forEach(r => {
        if(r.codigo === "ADMINISTRADOR" || r.codigo==="PROGRAMADOR"){
          this.hasAccess =  true;
        }
      });
    }
  }
  ngOnDestroy() {
    this.listenChangePeriods.unsubscribe();
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.professionalService.sortColumn = column;
    this.professionalService.sortDirection = direction;
  }
  // revisar centro de costos
  getProfessional(idPeriod): void {
    this.porcentajesView = false;
    let cc = JSON.parse(localStorage.getItem('Cc'));
    if (true) {
      
    }
    this.periodActual = idPeriod;
    this.spinner.show();
    this.professionalService.getProfessional(idPeriod, cc.id).subscribe((response: any) => {
      this.getPorcentajes();
      this.spinner.hide();

    }, (error) => {
      this.spinner.hide();
    });
  }
  openModal(content, nameProfessional, idProfessional, typeActivity) {
    this.spinner.show();
    this.nameProfessional = nameProfessional;
    this.idProfessional = idProfessional;
    this.typeActivity = typeActivity;
    this.modalService
      .open(content, { windowClass: 'modal-holder algoclass', backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'xl' })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeModal() {
    this.modalService.dismissAll();
    this.getPorcentajes();
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);
  }
  getPorcentajes() {
    this.porcentajesView = false;
    this.professionalService.getProgrammingPorcentage(this.periodActual).subscribe((response: any) => {
      this.porcentajesView = true;
    }, (error) => {
      this.spinner.hide();
    });
  }
}

