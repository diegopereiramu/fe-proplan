import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { SynchronizerService } from 'src/app/core/services/synchronizer/synchronizer.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ExcelService } from 'src/app/core/services/excel/excel.service';
import * as XLSX from 'xlsx'; 
@Component({
  selector: 'app-synchronizer',
  templateUrl: './synchronizer.component.html',
  styleUrls: ['./synchronizer.component.scss']
})
export class SynchronizerComponent implements OnInit {

  @ViewChild('modal', {static: false}) modal: ElementRef;

  fileToUpload: File = null;
  mensajeError = null;
  mensajeOK = null;
  fileName= 'ssbb-template-rrhh.xlsx';
  
  constructor(private synchronizer: SynchronizerService, private spinner: NgxSpinnerService, private modalService: NgbModal, private excelService: ExcelService) { }

  ngOnInit(): void {
    this.loadScript()
  }

  sincronizarPlanilla(){
    this.spinner.show();
    this.synchronizer.synchronize(this.fileToUpload).subscribe(data => {
        this.spinner.hide();
        this.mensajeOK = "Cargado correctamente";
        this.openModal();
      }, error => {
        this.spinner.hide();
        this.mensajeError = "Error";
        this.openModal();
        console.log(error);
      });
  }

  setFile(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  openModal(){
    let ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      keyboard : false
    };
    this.modalService.open(this.modal, ngbModalOptions);
  }

  closeModal(){
    this.modalService.dismissAll();
  }
  

  exportexcel(): void {
    this.excelService.getExcelData().subscribe((res: any) => {
      if(res.OK){
        const ws: XLSX.WorkSheet =XLSX.utils.aoa_to_sheet(res.programacion);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(wb, this.fileName);
      }
    });
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }
}
