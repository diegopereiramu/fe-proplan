import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { user } from './../../core/models/user/user';
import { AuthService } from './../../core/services/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Periods } from './../../core/models/periods/periods';
import { PeriodsService } from './../../core/services/periods/periods.service';
import { CostCenterService } from './../../core/services/costCenter/cost-center.service';
import { CostCenter } from './../../core/models/costCenter/costCenter';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public submitted: boolean = false;
  public alertSubmitError: boolean = false;
  public loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private route: Router,
    private periodsService: PeriodsService,
    private costCenterService: CostCenterService
  ) { }

  ngOnInit(): void {
    this.createFormLogin();
  }
  createFormLogin() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required,],
      password: ['', Validators.required,]
    });
  }
  login(): void {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    if (this.loginForm.valid) {
      this.spinner.show();
      this.authService.auth(this.loginForm.value.email, this.loginForm.value.password).subscribe((response: any) => {
        if (!response.OK) {
          this.alertSubmitError = true;
          this.spinner.hide();
        }
        else {
          let isAdmin = false;
          response.data.roles.forEach(roles => {
            if (roles.id == 1) {
              isAdmin = true;
            }
          });
          const promisePeriods = this.periodsService.getPeriods().toPromise();
          const promise2CC = this.costCenterService.getCostCenterByUser(isAdmin, response.data.user.id).toPromise();
          Promise.all([promisePeriods, promise2CC]).then(([responsePeriods, responseCC]) => {
            let periods: Periods[] = responsePeriods.periodos;
            localStorage.setItem('Periods', JSON.stringify(periods));
            let cc: CostCenter[] = responseCC.centro_costos;
            localStorage.setItem('Cc', JSON.stringify(cc));
            this.spinner.hide();
            this.route.navigateByUrl('/home');
          });
        }
      }, (error) => {
        this.spinner.hide();
        Swal.fire({
          title: 'Problemas de conexión',
          text: 'Consulte con el administrador de sistemas',
          icon: 'error',
          confirmButtonText: 'Cerrar'
        })
      });
    }
  }
}
