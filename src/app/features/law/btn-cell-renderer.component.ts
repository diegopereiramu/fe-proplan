import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';


@Component({
  selector: 'app-button-renderer',
  template: `
  <span *ngIf="!activo" style="color:#e82e39;font-weight:600"> Inactivo</span>

    <img *ngIf=!activo src="/assets/img/close.png"
  style="width: 16px;margin-left: 10px;cursor: pointer;" alt="">

  <span *ngIf="activo" style="color:#2dce89;font-weight:600"> Activo</span>
<img *ngIf=activo src="/assets/img/success.png" 
  style="width: 16px;margin-left: 10px;cursor: pointer;" alt="">
    `
})

export class BtnCellRenderer implements ICellRendererAngularComp {
  params;
  a:boolean=true;
  label: string;
  activo:boolean;
  agInit(params): void {
    this.params = params;
    this.label = this.params.label || null;
    if(params.data.activo==undefined)
    this.activo=params.data.estado
    else
    this.activo = params.data.activo
  }

  refresh(params?: any): boolean {
    return true;
  }

  onClick($event) {
    if (this.params.onClick instanceof Function) {
      const params = {
        event: $event,
        rowData: this.params.node.data
      }
      this.params.onClick(params);

    }
  }
}