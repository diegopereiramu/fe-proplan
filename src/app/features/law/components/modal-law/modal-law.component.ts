import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LeyService } from 'src/app/core/services/ley/ley.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-modal-law',
    templateUrl: './modal-law.component.html'
  })
  export class ModalLawComponent implements OnInit {

    @Input() law: any;
    @Output() closeModals = new EventEmitter<string>();
    @Output() saveLaw = new EventEmitter<any>();
    public titleModal: string = '';
    public textButton: string = '';
    public inputCodigo: string = '';
    public inputDescripcion: string = '';
    public inputTipoActividad: number = 0;
    public inputRendimiento: boolean = false;
    public inputHorasRendimiento: number = 0;
    public formError: boolean = false;
    public formErrorMsg: string;
    isNew: boolean;
    

    constructor(private spinner:NgxSpinnerService, public lawService: LeyService) { }

    ngOnInit(): void {
      if (this.law.id == null || this.law.id == '') {
        this.titleModal = 'Ingreso ley'
        this.textButton = 'Ingresar ley'
        this.isNew = true;
      } else {
        this.titleModal = 'Actualizar ley ' + this.law.descripcion;
        this.inputCodigo = this.law.code;
        this.inputDescripcion = this.law.descripcion;
        this.textButton = 'Actualizar ley'
        this.isNew = false;
      }
    }

    closeModal() {
        this.closeModals.emit();
    }

    addLaw(){
      if (this.law.codigo.trim() == '' || this.law.codigo == null || this.law.codigo == undefined) {
        this.formErrorshow('Ingrese un codigo por favor');
      } else if (this.law.descripcion == '' || this.law.descripcion == null || this.law.descripcion == undefined) {
        this.formErrorshow('Ingrese una descripción por favor');
      } else {
          this.saveLaw.emit(this.law);
      }
    }

    formErrorshow(msg) {
      this.formErrorMsg = msg;
      this.formError = true;
      // setTimeout(()=>{ this.formError = false}, 1500);
    }

    inactiveLaw(id){
      Swal.fire({
        title: '¿Estás seguro de desactivar la ley?',
        text: "Si la desactivas no se podra ver en la programación",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Desactivar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.lawService.deleteLaw(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Desactivado!',
              'La ley ha sido desactivada correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en deleteCostCenter: ", error)
            this.spinner.hide();
            this.closeModal();
          });
        }
      })
    }
  
    activeLaw(id){
      Swal.fire({
        title: '¿Estás seguro de activar la ley?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Activar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.lawService.activeLaw(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Activar!',
              'La ley ha sido activada correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en deleteCostCenter: ", error)
            this.spinner.hide();
            this.closeModal();
          });
        }
      })
    }
  }