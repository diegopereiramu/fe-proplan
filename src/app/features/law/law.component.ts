import { Component, OnInit } from '@angular/core';
import { LeyService } from '../../core/services/ley/ley.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { BtnCellRenderer } from './btn-cell-renderer.component';
import { distribucionHorasRenderer } from './distribucion_horas';

@Component({
  selector: 'app-law',
  templateUrl: './law.component.html',
  styleUrls: ['./law.component.scss'],
  providers: [LeyService]
})

export class LawComponent implements OnInit {

  laws: any[] = [];
  frameworkComponents: any;
  law: any = {id: '', descripcion: '', codigo: '', distribucion_horas: false};
  columnas=[{headerName: "Numero de Ley", field: "codigo", sortable: true, sort: 'asc',filter: 'agTextColumnFilter' },
  {headerName: "descripcion", field: "descripcion"},
  {
    headerName: 'distribucion horas',
    cellRenderer: 'distribucionHorasRenderer',
    cellRendererParams: {
     
      label: 'Click 1',
    }
  },
  {
    headerName: 'Activo',
    cellRenderer: 'buttonRenderer',
    cellRendererParams: {
     
      label: 'Click 1',
    }
  }
];
  maxPaginatorSize: number = 6;
  closeResult: any;
  pageSize: number = 6;
  page: number = 1;
  gridApi: any;
  gridColumnApi: any;
  

  constructor(private spinner:NgxSpinnerService, public lawService: LeyService, private modalService: NgbModal)
   {
      
  }
  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }
  ngOnInit(): void {
    this.frameworkComponents = {
      buttonRenderer: BtnCellRenderer,
      distribucionHorasRenderer:distribucionHorasRenderer
    }
    this.loadLaws();
    this.loadScript();
  }
  
  openModal(modal, law?) {
    this.law = {
      id: law.id, 
      descripcion: law.descripcion, 
      codigo: law.codigo,
      distribucion_horas: law.distribucion_horas,
      activo: law.activo
    };
    this.modalService
    .open(modal, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }

  closeModal(){
    this.modalService.dismissAll();
    this.loadLaws();
  }

  

  loadLaws(){
    this.spinner.show();
    this.lawService.getLawsCatalogue().subscribe((response: any) => {
      this.laws = response.leyes;
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

  openModalAddLaw(modal){
    this.law = {id: '', descripcion: '', codigo: '', distribucion_horas: false};
    this.modalService
    .open(modal, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  saveLaw(data){
    this.spinner.show();
    if(data.id == null || data.id == "") {
      this.lawService.addLaw(data).subscribe((response: any) => {
        this.spinner.hide();          
          Swal.fire(
            'Ingreso correcto!',
            'La ley se ha ingresado correctamente',
            'success'
          )
          this.loadLaws();
      }, (error) => {
        this.spinner.hide();
      });
    } else {
      this.lawService.updateLaw(data, data.id).subscribe((response: any) => {
        if(!response.OK) {
        this.spinner.hide();
          Swal.fire(
            'Actualización no completada!',
            'La ley no se ha actualizado correctamente',
            'error'
          )
        } else {          
          Swal.fire(
            'Actualizacion correcta!',
            'La ley se ha actualizado correctamente',
            'success'
          )
          this.loadLaws();
        }
      }, (error) => {
        this.spinner.hide();
      });
    }
    this.closeModal();
  }
  rowDataClicked1 = {};
  onBtnClick1(e) {
    alert("funciona");
  }
}