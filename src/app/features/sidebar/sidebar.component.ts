import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ComunicationService } from './../../core/services/comunication/comunication.service';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  public isApprover:boolean = false;
  public isObserver:boolean = false;
  public isProgrammer:boolean = false;
  public _opened: boolean = false;
  active = 'top';
  public isCollapsed:boolean;
  public isCollapsedProgramming:boolean;
  public isCollapsedCatalogue:boolean;
  public rol:number;

  constructor(private comunicationService:ComunicationService) {
    this.isCollapsed = true;
    this.isCollapsedProgramming = true;
    this.isCollapsedCatalogue = true;
    this.comunicationService.listencloseAndOpen().subscribe((idPeriodo: any) => {
      this._toggleOpened();
      },(error) => {
        (['comunicationService',error])
      });
  }
 
  ngOnInit(): void {
    let dataUser =  JSON.parse(localStorage.getItem('Personal-data'));
    dataUser.rols.forEach(element => {
      this.rol = element.id;
      if(element.id == 5 || element.id == 1) {
        this.isApprover = true;
      } else if(element.id == 4) {
        this.isObserver = true;
      } else {
        this.isProgrammer = true;
      }
    });
  }
  _toggleOpened() {
    this._opened = !this._opened;
  }
}
