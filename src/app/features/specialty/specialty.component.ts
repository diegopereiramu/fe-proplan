import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { Specialty } from './../../core/models/specialty/specialty';
import { SpecialtyService } from './../../core/services/specialty/specialty.service';
import { NgbdSortableHeader, SortEvent } from './../../core/directives/sortable.directive';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { BtnCellRenderer } from '../law/btn-cell-renderer.component';

@Component({
  selector: 'app-specialty',
  templateUrl: './specialty.component.html',
  styleUrls: ['./specialty.component.scss'], 
  providers: [SpecialtyService, DecimalPipe]
})
export class SpecialtyComponent implements OnInit {
  specialty$: Observable<Specialty[]>;
  total$: Observable<number>;
  public id:number = 0;
  specialty: any = {id: '', codigo: '', descripcion: "",activo:false};
  public code:string = '';
  public descripcion:string = '';
  public closeResult: any;
  especialidades:any[]=[];
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  columnas = [
    {headerName: "codigo", field: "codigo", sortable: true, sort: 'asc',filter: 'agTextColumnFilter' },
    {headerName: "descripcion", field: "descripcion"},
    {
      headerName: 'Activo',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
       
        label: 'Click 1',
      }
    }
  ];
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private modalService: NgbModal,
    public specialtyService: SpecialtyService,
    public spinner:NgxSpinnerService) {
    this.specialty$ = specialtyService.specialty$;
    this.total$ = specialtyService.total$;}

  ngOnInit(): void {
    this.loadScript();
    this.getSpecialty();
    this.frameworkComponents = {
      buttonRenderer: BtnCellRenderer,
    }
  }
  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);
  }
  getSpecialty():void {
    this.spinner.show();
    this.specialtyService.getSpecialtyCatalogue().subscribe((response: any) => {
      this.especialidades=response;
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }
  
  openModal(content,specialty?) {
    // this.spinner.show();
    this.specialty = {
      id: specialty.id, 
      descripcion: specialty.descripcion, 
      codigo: specialty.codigo,
      estado: specialty.estado
    };
    this.modalService
      .open(content, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  openModalAddSpecialty(modal){
    this.specialty = {id: '', descripcion: '', codigo: '', distribucion_horas: false};
    this.modalService
    .open(modal, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeModal() {
    this.modalService.dismissAll();
    this.getSpecialty();
  }
  saveSpecialty(data) {
    this.spinner.show();
    if(data.id == null) {
      this.specialtyService.addSpecialty(data).subscribe((response: any) => {
        this.spinner.hide();
          Swal.fire(
            'Ingreso correcto!',
            'La especialidad se ha ingresado correctamente',
            'success'
          )
          this.getSpecialty();
      }, (error) => {
        this.spinner.hide();
      });
    } else {
      this.specialtyService.updateSpecialty(data, data.id).subscribe((response: any) => {
        if(!response.OK) {
        this.spinner.hide();
          Swal.fire(
            'Actualización no completada!',
            'La especialidad no se ha actualizado correctamente',
            'error'
          )
        } else {
          this.getSpecialty();
          Swal.fire(
            'Actualizacion correcta!',
            'La especialidad se ha actualizado correctamente',
            'success'
          )
        }
        
      }, (error) => {
        this.spinner.hide();
      });
    }
    this.closeModal();
  }
}
function abValueGetter(params) {
  if(params.data.estado)
  return "esta si";
  else
  return "estas no";
}