import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSpecialtyComponent } from './modal-specialty.component';

describe('ModalSpecialtyComponent', () => {
  let component: ModalSpecialtyComponent;
  let fixture: ComponentFixture<ModalSpecialtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSpecialtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSpecialtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
