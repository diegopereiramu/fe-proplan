import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpecialtyService } from 'src/app/core/services/specialty/specialty.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-specialty',
  templateUrl: './modal-specialty.component.html',
  styleUrls: ['./modal-specialty.component.scss']
})
export class ModalSpecialtyComponent implements OnInit {
  @Input() specialty:any;
  @Output() closeModals = new EventEmitter<string>();
  @Output() saveSpecialty = new EventEmitter<any>();
  public titleModal: string = '';
  public textButton: string = '';
  public formError: boolean = false;
  public formErrorMsg: string;
  isNew: boolean;

  constructor(public specialtyService: SpecialtyService,
    public spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    if (this.specialty.id == null || this.specialty.id == '') {
      this.titleModal = 'Ingreso especialidad'
      this.textButton = 'Ingresar especialidad'
      this.isNew = true;
    } else {
      this.titleModal = 'Actualizar especialidad ' + this.specialty.descripcion;
      this.textButton = 'Actualizar especialidad'
      this.isNew = false;
    }
  }
  closeModal() {
    this.closeModals.emit();
  }
  addSpecialty() {
    if (this.specialty.codigo.trim() == '' || this.specialty.codigo == null || this.specialty.codigo == undefined) {
      this.formErrorshow('Ingrese un codigo por favor');
    } else if (this.specialty.descripcion == '' || this.specialty.descripcion == null || this.specialty.descripcion == undefined) {
      this.formErrorshow('Ingrese una descripción por favor');
    } else {
        let obj = {
          id: this.specialty.id,
          codigo: this.specialty.codigo,
          descripcion: this.specialty.descripcion
        }
        this.saveSpecialty.emit(obj);
    }
  }
  formErrorshow(msg) {
    this.formErrorMsg = msg;
    this.formError = true;
  }
  inactiveSpecialty(id) {
    Swal.fire({
      title: '¿Estás seguro de desactivar la especialidad?',
      text: "Si la desactivas no podras verla en las programaciones",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Desactivar',
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.specialtyService.deleteSpeciality({estado: 0}, id).subscribe((response: any) => {
          this.spinner.hide();
          Swal.fire(
            'Desactivado!',
            'La programación ha sido desactivada correctamente',
            'success'
          )
          this.closeModal();
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }

  activeSpecialty(id) {
    Swal.fire({
      title: '¿Estás seguro de activar la especialidad?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Activar!',
      cancelButtonText: 'No, Cancelar',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.specialtyService.deleteSpeciality({estado: 1}, id).subscribe((response: any) => {
          this.spinner.hide();
          Swal.fire(
            'Activado!',
            'La programación ha sido activada correctamente',
            'success'
          )
          this.closeModal();
        }, (error) => {
          this.spinner.hide();
        });
      }
    })
  }

}
