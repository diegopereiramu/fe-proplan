import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { user } from 'src/app/core/models/user/user';
import { ComunicationService } from '../../core/services/comunication/comunication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {
  public userName:string;
  public periods:any;
  public idSelectPeriods:any = 0;

  constructor(private comunicationService:ComunicationService) {
    let dataUserLocalStorage = localStorage.getItem('Personal-data');
    if(dataUserLocalStorage) {
      this.userName = JSON.parse(dataUserLocalStorage)['name'];
    }
    let periods =  JSON.parse(localStorage.getItem('Periods'));
    if(periods) {
      periods.forEach(element => {
          if(element.actual){this.idSelectPeriods = element.id}
      });
      this.periods = periods;
    }
  }

  ngOnInit(): void {
  }
  changePeriods(event) {
    if(this.idSelectPeriods != 0) {
      this.periods.forEach(element => {
        if(element.id == this.idSelectPeriods){element.actual = true} else {element.actual = false} 
      });
      localStorage.setItem('Periods', JSON.stringify(this.periods));
      this.comunicationService.listenChangePeriods(this.idSelectPeriods);
    }
  }
  cleanSesion() {
    localStorage.clear();
  }
  hideAndShowSidebar() {
    this.comunicationService.closeAndOpen();
  }

}
