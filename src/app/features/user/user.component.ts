import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/core/services/user/user.service';
import { BtnCellRenderer } from '../law/btn-cell-renderer.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  datos=[];
  dato;
  closeResult: string;
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  columnas =[
      {headerName: "nombre", field: "nombre", sortable: true, sort: 'asc',filter: 'agTextColumnFilter' },
      {headerName: "usuario", field: "usuario"},
      {
        headerName: 'Activo',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
         
          label: 'Click 1',
        }
      },
      {headerName: "rol", field: "descripcion"},
      {headerName: "centro de costo", field: "centro_costo"}
    ];
  constructor(private UserService:UserService,private modalService: NgbModal) { }
 
  @Output() registro = new EventEmitter<any>();
  ngOnInit(): void {
    this.frameworkComponents = {
      buttonRenderer: BtnCellRenderer,
    }
    this.ActualizarDatos();
    this.loadScript();
  }
  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
  ActualizarDatos()
  {
    this.UserService.getUsers().subscribe(hd => {
      if(hd){
        this.datos=hd;
      }
    });
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }
  openModal(modalActivity,datos?) {
    this.dato=datos;
    this.modalService
      .open(modalActivity, { windowClass: 'modal-holder', backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
      .result.then(
        result => {
          this.closeResult = `Closed with: ${result}`;
        },
        reason => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      ).then(
        () => {
          this.ActualizarDatos();
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  closeModal() {
    this.modalService.dismissAll();
  }

  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }
}
