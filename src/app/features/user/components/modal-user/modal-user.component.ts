import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { user2 } from 'src/app/core/models/user/user';
import { CostCenterService } from 'src/app/core/services/costCenter/cost-center.service';
import { RolService } from 'src/app/core/services/rol/rol.service';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-modal-user',
  templateUrl: './modal-user.component.html',
  styleUrls: ['./modal-user.component.scss']
})
export class ModalUserComponent implements OnInit {
  roles=[];
  ccs=[];
  @Input() registro:user2;
  @Output() closeModals = new EventEmitter<string>();
  public titleModal: string = '';
  isNew = false;
  public formError: boolean = false;
  public formErrorMsg: string;

  constructor(private UserService:UserService,private RolService:RolService, private CostCenterService:CostCenterService,private toastr: ToastrService) { }
 
  ngOnInit(): void {
    if (this.registro === undefined) {
      this.isNew = true;
      this.titleModal = "Nuevo registro de usuario";
      this.registro = {
        id:0,
        nombre:"",
        usuario:"",
        activo:"",
        rol:0,
        centro_costo:0
        }           
      }
    else{
      this.titleModal = this.registro.nombre;
    }
  this.RolService.getRoles().subscribe(n => {
    this.roles = n;
  });
this.CostCenterService.getCostCenterCatalogue().subscribe(n => {
  this.ccs = n.centro_costos;
});
  
}
closeModal() {
  this.closeModals.emit();
}
formErrorshow(msg) {
  this.formErrorMsg = msg;
  this.formError = true;
  //  setTimeout(()=>{ this.formError = false}, 1500);
}

saveUpdate() {
  if (this.registro.nombre == undefined|| this.registro.nombre.trim() == '') {
    this.showWarning('Falta nombre');
  } else if (this.registro.centro_costo==undefined || this.registro.centro_costo==0) {
    this.showWarning('Falta centro de costo de usuario');
  }
  else if (this.registro.usuario==undefined || this.registro.usuario.trim() == '' ) {
    this.showWarning('Falta nombre de usuario');
  }
  else if (this.registro.rol==undefined || this.registro.rol==0) {
    this.showWarning('Falta rol de usuario');
  }
  if (!this.registro.nombre) {
    this.formErrorshow('Favor de completar todos los campos');
  } 
  else {
    if (this.isNew) {
      // crear nuevo registro
      this.UserService.postUser(this.registro).subscribe(hd => {
        if(hd){
          this.closeModal();
        }
      });
    }
    else{
        this.UserService.putUser(this.registro).subscribe(hd => {
          if(hd){
            this.closeModal();
          }
        });
      }
  }
}

delete() {
 this.UserService.deleteUser(this.registro).subscribe(hd => { 
  if(hd){
      
      this.closeModal();
    }
  });

}

showWarning(msg) {
  this.toastr.error(msg, "Faltan datos", {
    positionClass: "toast-bottom-center"
  });
}
}
