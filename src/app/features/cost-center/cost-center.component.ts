import { Component, OnInit } from '@angular/core';
import { CostCenterService } from '../../core/services/costCenter/cost-center.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { BtnCellRenderer } from '../law/btn-cell-renderer.component';

@Component({
  selector: 'app-cost-center',
  templateUrl: './cost-center.component.html',
  styleUrls: ['./cost-center.component.scss'],
  providers: [CostCenterService]
})
export class CostCenterComponent implements OnInit {

  costCenters: any[] = [];
  costCenter: any = {id: '', descripcion: '', codigo: ''};
  maxPaginatorSize: number = 6;
  closeResult: any;
  pageSize: number = 6;
  page: number = 1;
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  columnas = [
    {headerName: "codigo", field: "codigo", sortable: true, sort: 'asc',filter: 'agTextColumnFilter' },
    {headerName: "descripcion", field: "descripcion"},
    {
      headerName: 'Activo',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
       
        label: 'Click 1',
      }
    }
  ];
  constructor(private spinner:NgxSpinnerService, public costCenterService: CostCenterService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.loadCostCenters();
    this.loadScript();
    this.frameworkComponents = {
      buttonRenderer: BtnCellRenderer,
    }
  }
  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.sizeToFit();
  }
  public loadScript() {
    let node3 = document.createElement("script");
    node3.type = "text/javascript";
    node3.async = true;
    node3.charset = "utf-8";
    node3.src = "assets/js/dashboard.js"
    document.getElementsByTagName("head")[0].appendChild(node3);

  }

  openModal(modal, costCenter) {
    this.costCenter = {
      id: costCenter.id, 
      descripcion: costCenter.descripcion, 
      codigo: costCenter.codigo,
      activo:costCenter.activo
    };
    this.modalService
    .open(modal, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  closeModal(){
    this.modalService.dismissAll();
    this.loadCostCenters();
  }

 

  loadCostCenters(){
    this.spinner.show();
    this.costCenterService.getCostCenterCatalogue().subscribe((response: any) => {
      this.costCenters = response.centro_costos;
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

  openModalAddCostCenter(modal){
    this.costCenter = {id: '', descripcion: '', codigo: ''};
    this.modalService
    .open(modal, { windowClass: 'modal-holder',backdrop: 'static', keyboard: false, ariaLabelledBy: 'modal-basic-title', centered: true, size: 'lg' })
    .result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  saveCostCenter(data){
    this.spinner.show();
    if(data.id == null || data.id == "") {
      this.costCenterService.addCostCenter(data).subscribe((response: any) => {
        this.spinner.hide();          
          Swal.fire(
            'Ingreso correcto!',
            'El centro de costos se ha ingresado correctamente',
            'success'
          )
          this.loadCostCenters();
      }, (error) => {
        this.spinner.hide();
      });
    } else {
      this.costCenterService.updateCostCenter(data, data.id).subscribe((response: any) => {
        if(!response.OK) {
        this.spinner.hide();
          Swal.fire(
            'Actualización no completada!',
            'El centro de costos no se ha actualizado correctamente',
            'error'
          )
        } else {          
          Swal.fire(
            'Actualizacion correcta!',
            'El centro de costos se ha actualizado correctamente',
            'success'
          )
          this.loadCostCenters();
        }
      }, (error) => {
        this.spinner.hide();
      });
    }
    this.closeModal();
  }
}
