import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CostCenterService } from 'src/app/core/services/costCenter/cost-center.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-modal-cost-center',
    templateUrl: './modal-cost-center.component.html'
  })
  export class ModalCostCenterComponent implements OnInit {

    @Input() costCenter: any;
    @Output() closeModals = new EventEmitter<string>();
    @Output() saveCostCenter = new EventEmitter<any>();
    public titleModal: string = '';
    public textButton: string = '';
    public inputCodigo: string = '';
    public inputDescripcion: string = '';
    public inputTipoActividad: number = 0;
    public inputRendimiento: boolean = false;
    public inputHorasRendimiento: number = 0;
    public formError: boolean = false;
    public formErrorMsg: string;
    isNew: boolean;

    constructor(private spinner:NgxSpinnerService, public costCenterService: CostCenterService) { }

    ngOnInit(): void {
      if (this.costCenter.id == null || this.costCenter.id == '') {
        this.titleModal = 'Ingreso centro de costo'
        this.textButton = 'Ingresar centro de costo'
        this.isNew = true;
      } else {
        this.titleModal = 'Actualizar centro de costo ' + this.costCenter.descripcion;
        this.inputCodigo = this.costCenter.code;
        this.inputDescripcion = this.costCenter.descripcion;
        this.textButton = 'Actualizar centro de costo'
        this.isNew = false;
      }
    }

    closeModal() {
        this.closeModals.emit();
    }

    addCostCenter(){
      if (this.costCenter.codigo.trim() == '' || this.costCenter.codigo == null || this.costCenter.codigo == undefined) {
        this.formErrorshow('Ingrese un codigo por favor');
      } else if (this.costCenter.descripcion == '' || this.costCenter.descripcion == null || this.costCenter.descripcion == undefined) {
        this.formErrorshow('Ingrese una descripción por favor');
      } else {
          this.saveCostCenter.emit(this.costCenter);
      }
    }

    formErrorshow(msg) {
      this.formErrorMsg = msg;
      this.formError = true;
      // setTimeout(()=>{ this.formError = false}, 1500);
    }
    inactiveCostCenter(id){
      Swal.fire({
        title: '¿Estás seguro de desactivar el centro de costos?',
        text: "Si la desactivas no se podra ver en la programación",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Desactivar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.costCenterService.deleteCostCenter(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Desactivado!',
              'El centro de costo ha sido desactivado correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en inactiveCostCenter: ", error)
            this.spinner.hide();
          });
        }
      })
    }
  
    activeCostCenter(id){
      Swal.fire({
        title: '¿Estás seguro de activar el centro de costos?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Activar',
        cancelButtonText: 'No, Cancelar',
      }).then((result) => {
        if (result.value) {
          this.spinner.show();
          this.costCenterService.activeCostCenter(id).subscribe((response: any) => {
            this.spinner.hide();
            Swal.fire(
              'Activado!',
              'El centro de costo ha sido activado correctamente',
              'success'
            )
            this.closeModal();
          }, (error) => {
            console.error("Error en activeCostCenter: ", error)
            this.spinner.hide();
          });
        }
      })
    }
  }