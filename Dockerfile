
### STAGE 1: Build ###

FROM node:10-alpine as builder

COPY package.json ./

RUN npm i && mkdir /ng-app && mv ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .

RUN $(npm bin)/ng build --prod --build-optimizer

### STAGE 2: Setup ###

FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /ng-app/dist/fe-proplan /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
